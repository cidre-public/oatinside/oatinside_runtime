#include "hook_table_allocator.h"

#include <sys/mman.h>
#include <sys/user.h>
#include <unistd.h>

#include "utils.h"

namespace art {

  void* HookTableAllocator::mmap_new_page() {
    void* page_addr = mmap(NULL, PAGE_SIZE, PROT_READ | PROT_WRITE | PROT_EXEC, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
    void *prev = last;

    if(first_mmap && last_mmap) {
      *(void**)last_mmap = page_addr; // Chain mmap
      last_mmap = page_addr; // Update current
    } else {
      // Init mmap list
      first_mmap = page_addr;
      last_mmap = page_addr;
    }
    *(void**)page_addr = NULL; // End of mmap list
    page_addr = (void*)((unsigned char*)page_addr + sizeof(void*)); // Reserve place for mmap list pointer

    // For __arm__ align entry on 4 (0b100) for thumb and jazelle encoding
#if defined(__arm__)
    for(unsigned long i=0 ; i + sizeof(struct allocated_entry) - 1 < (unsigned long)(PAGE_SIZE-sizeof(void*)) ; i+=sizeof(struct allocated_entry) + 4 - (sizeof(struct allocated_entry)%4) ) {
#else
    for(unsigned long i=0 ; i + sizeof(struct allocated_entry) - 1 < (unsigned long)(PAGE_SIZE-sizeof(void*)) ; i+=sizeof(struct allocated_entry) ) {
#endif 
      *(void**)((char*)page_addr + i) =  prev;
      prev = (void*)((char*)page_addr + i);
    }

    void* newLast = prev;

    return newLast;
  }

  void* HookTableAllocator::alloc() {
    pthread_mutex_lock(&allocator_mutex);
    if( last == 0 ) {
      last = mmap_new_page();
    }

    void* oldLast = last;

    last = *(void**)last;
    pthread_mutex_unlock(&allocator_mutex);
    return oldLast;
  }

  void HookTableAllocator::fill_entry(struct allocated_entry* entry , void* quick_code, ArtMethod* method, const uint8_t* begin_, uint32_t code_offset_, HookType type, bool thumb) {
#if defined(__arm__)
    if(thumb) {
      entry->instr[3] = '\xbe';
      entry->instr[2] = '\xbe';
      entry->instr[1] = '\xbe';
      entry->instr[0] = '\xbe';
    } else {
      entry->instr[3] = '\xe1';
      entry->instr[2] = '\x20';
      entry->instr[1] = '\x00';
      entry->instr[0] = '\x70';
    }
#elif defined(__aarch64__)
    entry->instr[3] = '\xd4';
    entry->instr[2] = '\x20';
    entry->instr[1] = '\x00';
    entry->instr[0] = '\x00';
#endif

    entry->quick_code_addr = quick_code;
    entry->method = method;
    entry->begin_ = begin_;
    entry->code_offset_ = code_offset_;
    entry->thumb = thumb;

    entry->flag = ~(*((unsigned int*)entry->quick_code_addr));

    entry->hook_type = type;

    FlushInstructionCache((char*)entry, (char*)entry+4 + sizeof(void*));
  }

  void HookTableAllocator::free(void* addr) {
    pthread_mutex_lock(&allocator_mutex);
    *(void**) addr = last;
    last = addr;
    pthread_mutex_unlock(&allocator_mutex);
  }

  bool HookTableAllocator::checkFlag(struct allocated_entry* entry, bool safe_check) {
    if(safe_check) {
      bool is_in_mmap_list = false;
      for(void* current_mmap=first_mmap ; current_mmap!=NULL ; current_mmap=*(void**)current_mmap) {
	if(current_mmap <= (void*)entry && entry < (void*)((unsigned char*)current_mmap + PAGE_SIZE)) {
	  is_in_mmap_list = true;
	  break;
	}
      }
      if(!is_in_mmap_list)
	return false;
    }
    if(entry && entry->flag == ~(*((unsigned int*)entry->quick_code_addr)))
      return true;
    return false;
  }

}
