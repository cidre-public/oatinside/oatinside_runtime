#include "probe_manager.h"

#include <arpa/inet.h>
#include <backtrace/Backtrace.h>
#include <backtrace/BacktraceMap.h>
#include <dlfcn.h>
#include <errno.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/socket.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>
#include <ucontext.h>

#include <google/protobuf/io/zero_copy_stream_impl.h>
#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/io/zero_copy_stream_impl_lite.h>

#include "base/logging.h"
#include "entrypoints/quick/callee_save_frame.h"
#include "event_proto2.pb.h"
#include "hook_table_allocator.h"
#include "jni.h"
#include "mirror/object-inl.h"
#include "mirror/class-inl.h"
#include "mirror/string.h"
#include "oat_file-inl.h"
#include "thread.h"
#include "utils.h"

namespace art {
  ProbeManager::ProbeManager() :
    debugThisProc(false),
    alreadyCheckDebug(0),
    lastUid(0),
    dumpedMethod(nullptr),
    lastUidForDumpCheck(0),
    vector_mutex(PTHREAD_MUTEX_INITIALIZER),
    event(new protobuf_log::Event()),
    readEv(new protobuf_log::Event_Read()),
    invokeEv(new protobuf_log::Event_Invoke()),
    writeEv(new protobuf_log::Event_Write()),
    retEv(new protobuf_log::Event_Ret()),
    newEv(new protobuf_log::Event_NewObj()),
    throwEv(new protobuf_log::Event_Throw()),
    catchEv(new protobuf_log::Event_Catch()),
    loadEv(new protobuf_log::Event_MethodLoad()),
    monitorEv(new protobuf_log::Event_Monitor()),
    getArrayEv(new protobuf_log::Event_GetArray()),
    lengthArrayEv(new protobuf_log::Event_LengthArray()),
    valueEv({}),
    socket_mutex(PTHREAD_MUTEX_INITIALIZER) {
    // For debug purpose
    pthread_mutexattr_t attr;
    pthread_mutexattr_init(&attr);
    pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_ERRORCHECK);
    pthread_mutex_init(&vector_mutex, &attr);
  }

  ProbeManager::~ProbeManager() {
    if(socket_fd != 1) {
      close(socket_fd);
      socket_fd = -1;
    }
  }

  template <typename EventType>
  void ProbeManager::AllocateUntil(std::vector<EventType*> &v, int i) {
    int diff = i + 1 - v.size();
    for(int j=0 ; j < diff ; j++)
      v.push_back(new EventType());
  }

  void ProbeManager::FillProtoValue(void* value_addr, const char* descriptor, protobuf_log::Event_Value* value) {
    switch(descriptor[0]) {
    case 'D':
      value->set_do_(*(double*)value_addr);
      break;
    case 'F':
      value->set_fl(*(float*)value_addr);
      break;
    case 'I':
    case 'S':
      value->set_i32(*(int32_t*)value_addr);
      break;
    case 'J':
      value->set_i64(*(int64_t*)value_addr);
      break;
    case 'Z':
      value->set_bo(*(bool*)value_addr);
      break;
    case 'B':
    case 'C':
      value->set_by((uint8_t*)value_addr, 1);
      break;
    case 'L':
    case '[':
      if(!strcmp(descriptor, "Ljava/lang/String;") && value_addr != NULL) {
	mirror::String* mirrorString = (mirror::String*)((unsigned long long)(*(mirror::String**)value_addr) & 0xffffffff);
	if(mirrorString) {
	  // GLOG(INFO, "[1] " << (void*) mirrorString);
	  // GLOG(INFO, "[3] " << mirrorString->ToModifiedUtf8());
	  value->set_st(mirrorString->ToModifiedUtf8());
	} else
	  value->set_st("[NULL]");
      } else
	value->set_ui64((unsigned long long)value_addr);
      break;
    case 'V':
      value->set_i32(0);
      break;
    default:
      GLOG(INFO, "ERROR1: unknown type for descriptor \"" << descriptor << "\"");
      value->set_ui64(0);
      value->set_type("Lfail/to/retrieve");
      return;
    }
    value->set_type(descriptor);
  }

  void ProbeManager::FillProtoValue(unsigned long value_addr, const char* descriptor, protobuf_log::Event_Value* value) {
    uint8_t uint8_tmp;
    switch(descriptor[0]) {
    case 'D':
      value->set_do_((double)value_addr);
      break;
    case 'F':
      value->set_fl((float)value_addr);
      break;
    case 'I':
    case 'S':
      value->set_i32((int32_t)(value_addr & 0xffffffff));
      break;
    case 'J':
      value->set_i64(value_addr);
      break;
    case 'Z':
      value->set_bo((bool)value_addr);
      break;
    case 'B':
    case 'C':
      uint8_tmp = (uint8_t)(value_addr & 0xff);
      value->set_by(&uint8_tmp, 1);
      break;
    case 'L':
    case '[':
      if(!strcmp(descriptor, "Ljava/lang/String;") && value_addr != 0) {	
	mirror::String* mirrorString = (mirror::String*)(value_addr & 0xffffffff);
	if(mirrorString) {
	  // GLOG(INFO, "[1] " << (void*) mirrorString);
	  // GLOG(INFO, "[3] " << mirrorString->ToModifiedUtf8());
	  value->set_st(mirrorString->ToModifiedUtf8());
	} else
	  value->set_st("[NULL]");
      } else
	value->set_ui64(value_addr);
      break;
    case 'V':
      value->set_i32(0);
      break;
    default:
      GLOG(INFO, "ERROR2: unknown type for descriptor \"" << descriptor << "\"");
      value->set_ui64(0);
      value->set_type("Lfail/to/retrieve");
      return;
    }
    value->set_type(descriptor);
  }

  char bytes[2048];
  void ProbeManager::LogEventToSocket( // __attribute__((__unused__)) 
				      protobuf_log::Event* logged_event) {
    if(socket_fd != -1) {
      int size = logged_event->ByteSize();
      pthread_mutex_lock(&socket_mutex);
      struct timespec start, end;
      clock_gettime(CLOCK_MONOTONIC_RAW, &start);
      if(write(socket_fd, &size, sizeof(int)) == -1){
      	pthread_mutex_unlock(&socket_mutex);
      	GLOG(INFO, "ERROR write: " << strerror(errno));
      	while(1);
      }	else {
    	// logged_event->SerializeToFileDescriptor(socket_fd);
    	std::string str;
    	logged_event->SerializeToString(&str);
    	const char* c = str.c_str(); 
    	if(write(socket_fd, c, size) == -1){
    	  pthread_mutex_unlock(&socket_mutex);
    	  GLOG(INFO, "ERROR write: " << strerror(errno));
    	  while(1);
    	}
      }
      clock_gettime(CLOCK_MONOTONIC_RAW, &end);
      unsigned int diff = (end.tv_sec - start.tv_sec) * 1000000000 + (end.tv_nsec - start.tv_nsec);
      if(time_to_count) time_protobuf += diff;
      pthread_mutex_unlock(&socket_mutex);

      if(time_to_count) {
	switch(logged_event->event_data_case()) {
	case protobuf_log::Event::EventDataCase::kRead:
	  nb_read++;
	  break;
	case protobuf_log::Event::EventDataCase::kWrite:
	  nb_write++;
	  break;
	case protobuf_log::Event::EventDataCase::kInvoke:
	  nb_invoke++;
	  break;
	case protobuf_log::Event::EventDataCase::kRet:
	  nb_ret++;
	  break;
	case protobuf_log::Event::EventDataCase::kNewObj:
	  nb_new++;
	  break;
	case protobuf_log::Event::EventDataCase::kThrow:
	  nb_throw++;
	  break;
	case protobuf_log::Event::EventDataCase::kCatch:
	  nb_catch++;
	  break;
	case protobuf_log::Event::EventDataCase::kMethodLoad:
	  nb_load++;
	  break;
	default:
	  break;
	}
      }
    }
  }

  void ProbeManager::visitCallback(mirror::Object* obj, void* checked_obj)
    NO_THREAD_SAFETY_ANALYSIS {

    if((void*)obj <= checked_obj && checked_obj <= ((unsigned char*)obj) + obj->SizeOf() ) {
      ProbeManager* pm  = Runtime::Current()->GetProbeManager();
      mirror::Class* cls = obj->GetClass();

      pm->touched_object = obj;
      pm->touched_class = cls;

      unsigned int offset = ((unsigned char*)checked_obj) - ((unsigned char*)obj);

      pm->touched_field = NULL;
      IterationRange<StrideIterator<ArtField>> ifields = cls->GetIFields();
      for(ArtField& field : ifields) {
	if(field.GetOffset().Uint32Value() == offset) {
	  pm->touched_field =  &field;
	}
      }
    }
  }

  /* TODO: give the OatMethod in argument of the hook
   * If the method is native, give NULL ?
   */
  class HookClassVisitor : public art::ClassVisitor {
  public:
    HookClassVisitor(size_t pointer_size)
      : ptr_size(pointer_size) {}

    bool operator()(mirror::Class* klass) OVERRIDE SHARED_REQUIRES(Locks::mutator_lock_) {
      mirror::DexCache* dex_cache = klass->GetDexCache();

      if(dex_cache != nullptr) {
      	const DexFile* dex_file = dex_cache->GetDexFile();
      	const OatFile::OatDexFile* oat_dex_file = dex_file->GetOatDexFile();

      	if (oat_dex_file != nullptr) {
      	  size_t class_def_index = klass->GetDexClassDefIndex();
      	  OatFile::OatClass oat_class = oat_dex_file->GetOatClass(class_def_index);

      	  for(auto& m : klass->GetDirectMethods(ptr_size)) {

      	    if(m.HasAnyCompiledCode() && !m.IsNative()) {

      	      uint16_t class_def_method_index = m.GetMethodIndex();

      	      if(oat_class.GetOatMethodOffsets(class_def_method_index)) {
      		const OatFile::OatMethod oat_method = oat_class.GetOatMethod(class_def_method_index);

      		bool thumb = oat_class.GetOatFile()->GetOatHeader().GetInstructionSet() == kThumb2;
      		Runtime::Current()->GetProbeManager()->OatMethodLinked(&m, oat_method.GetBegin_(), oat_method.GetCodeOffset_(), oat_method.GetQuickCode(), thumb);
      	      }
      	    }
      	  }
      	}
      }

      return true;
    }
    size_t ptr_size;
  };


  bool ProbeManager::IsProcDebug() {
    debugThisProc = false;
    unsigned int current_uid = geteuid();
    static bool probe_and_signal_manager_already_init = false;

    if(IsProcDump()) {
      debugThisProc = true;
      return true;
    }
    
    if(current_uid != lastUid) {
      lastUid = current_uid;

      FILE* file = fopen("/data/local/tmp/traced_uid", "r");
      if(file != NULL) {
    	unsigned int uid;
    	int ret;
    	do {
    	  ret = fscanf(file, "%i\n", &uid);
    	  if (ret == 1 && uid == current_uid) {
    	    break;
    	  }
    	} while (ret != EOF);
    	if (uid == current_uid) {
    	  alreadyCheckDebug = 2;

    	  if(!probe_and_signal_manager_already_init) {
    	    if(IsProcDebug()) {
    	      probe_and_signal_manager_already_init = true;
    	      Runtime::Current()->GetSignalManager()->Init();
    	      Init();
    	    }
    	  }

	  if(!Thread::Current()->IsExceptionPending()) {
	    /* We have to hook already loaded OAT methods */
	    ClassLinker* cl = Runtime::Current()->GetClassLinker();
	    HookClassVisitor cl_visitor(cl->GetImagePointerSize());
	    cl->VisitClassesWithoutClassesLock(&cl_visitor);

	    /* Disable already alocated pages */
	    Runtime::Current()->GetHeap()->DisableHeap();

	    // GLOG(INFO, "End");
	  }
    	}
    	else {
    	  alreadyCheckDebug = 1;
    	}
    	fclose(file);
      } else {
    	GLOG(INFO, "Cannot open file !!!");
    	alreadyCheckDebug = 1;
      }
    }

    if(alreadyCheckDebug==2) {
      debugThisProc = true;
    }
    
    return debugThisProc;
  }

  

  bool ProbeManager::IsProcDump() {
    static bool probe_and_signal_manager_already_init = false;
    static bool last_return = false;
    unsigned int current_uid = geteuid();
    char* line_buf;
    unsigned int line_buf_size = 1;
    unsigned int method_name_size = 0;
    ssize_t read_size = 0;

    if(current_uid != lastUidForDumpCheck) {
      lastUidForDumpCheck = current_uid;

      FILE* file = fopen("/data/local/tmp/dumped_uid", "r");
      if(file != NULL) {
    	unsigned int uid;
    	int ret;

	line_buf = (char*) malloc(1*sizeof(char));
	
    	while(true) {

	  ret = fscanf(file, "%i %u\n", &uid, &method_name_size);
	  if(ret == EOF)
	    break;
	  
	  if(line_buf_size < method_name_size + 2) {
	    line_buf_size = method_name_size+2;
	    line_buf = (char*) realloc(line_buf, line_buf_size);
	  }
	  read_size = fread(line_buf, 1, method_name_size+1, file);
	  line_buf[read_size-1] = '\0'; // remove trailing '\n' and terminate char*

	  if (uid == current_uid) {
	    last_return = true;

	    if(!probe_and_signal_manager_already_init) {
    	      probe_and_signal_manager_already_init = true;

	      Runtime::Current()->GetSignalManager()->Init();
	      Init();
	    }

	    /* We have to hook already loaded OAT methods */
	    ClassLinker* cl = Runtime::Current()->GetClassLinker();
	    HookClassVisitor cl_visitor(cl->GetImagePointerSize());
	    cl->VisitClassesWithoutClassesLock(&cl_visitor);

	    /* Disable already alocated pages */
	    Runtime::Current()->GetHeap()->DisableHeap();

	    dumpedMethod = line_buf;
	    fclose(file);
	    return true;
    	  }
    	}
	last_return = false;
	free(line_buf);
    	fclose(file);
      }
    }

    return last_return;
  }

  bool ProbeManager::IsDumpedMethod(ArtMethod* method) {
    const char* clsDesc = method->GetDeclaringClassDescriptor();
    size_t clsDescSize = strlen(method->GetDeclaringClassDescriptor());
    if(!strncmp(clsDesc, dumpedMethod, clsDescSize)
       && !strcmp(method->GetName(), dumpedMethod + clsDescSize)) {
      return true;
    } else {
      return false;
    }
  }

  /* Have to be called AFTER signalManager is initialized */
  void ProbeManager::Init() {
    /* Init logging socket */
    socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if(socket_fd != -1) {
      int optval = 1;
      if(setsockopt(socket_fd, SOL_SOCKET, SO_KEEPALIVE, &optval, sizeof(optval)) != -1) {
	struct sockaddr_in addr;

	/* man 7 ip */
	memset(&addr, 0, sizeof(addr));
	addr.sin_family = AF_INET;
	addr.sin_port = htons(LOGGING_PORT_NUMBER);
	addr.sin_addr.s_addr = htonl(INADDR_LOOPBACK);

	if(connect(socket_fd, (struct sockaddr*)&addr, sizeof(addr)) == -1) {
	  GLOG(INFO, "ERROR connect: " << strerror(errno));
	  close(socket_fd);
	  socket_fd = -1;
	}
      } else {
	GLOG(INFO, "ERROR setsockopt: " << strerror(errno));
	close(socket_fd);
	socket_fd = -1;
      }
    } else {
      GLOG(INFO, "ERROR socket: " << strerror(errno));
    }
  }

  void ProbeManager::OatMethodLinked(ArtMethod* method, const uint8_t* begin_, uint32_t code_offset_, const void* quick_code, bool thumb) {
    if(IsProcDebug()) {
      // GLOG(INFO, "OatMethodLinked");
      if(quick_code) {
	// Prevent to hook zygote function that are already running when switching into debug mode
	// causes crashes
	if(strncmp(method->GetDeclaringClassDescriptor(), "Lcom/android/internal/", 22) != 0) {	  
	  void* hook_addr = hook_table_allocator.alloc();
	  hook_table_allocator.fill_entry((struct HookTableAllocator::allocated_entry*)hook_addr, (void*)quick_code, method, begin_, code_offset_, HookType::invoke, thumb);
	  if(thumb)
	    method->SetEntryPointFromQuickCompiledCode((void*)((unsigned long)hook_addr|1));
	  else
	    method->SetEntryPointFromQuickCompiledCode(hook_addr);

	  // const void* event_addr_offset = method->GetDexFile()->GetOatDexFile()->GetOatFile()->Begin();
	  
	  // loadEv->set_caller_type(method->GetDeclaringClassDescriptor());
	  // loadEv->set_method_name(method->GetName());
	  // loadEv->set_event_addr_offset((const uint64_t)event_addr_offset);
	  // loadEv->set_method_code_offset((const uint64_t)((const char*)quick_code - (const char*)event_addr_offset));
	  // event->set_allocated_methodload(loadEv);
	  
	  // LogEventToSocket(event);
	  
	  // loadEv->release_caller_type();
	  // loadEv->release_method_name();
	  
	  // event->release_methodload();
	  }
	// GLOG(INFO, "stop linked");
      }
    }
  }

  uint64_t GetEventAddr(unsigned int size_of_instr = 4)
      SHARED_REQUIRES(Locks::mutator_lock_) {
    Runtime* runtime = Runtime::Current();
    Thread* thd = Thread::Current();
    for(const ManagedStack* ms = thd->GetManagedStack() ; ms != 0 ; ms = ms->GetLink()) {
      ArtMethod** sp = ms->GetTopQuickFrame();
      if(sp) {
	for (int i = 0; i < Runtime::kLastCalleeSaveType; i++) {
	  Runtime::CalleeSaveType type = Runtime::CalleeSaveType(i);
	  if (*sp == runtime->GetCalleeSaveMethod(type)) {
	    const size_t callee_return_pc_offset = GetCalleeSaveReturnPcOffset(runtime->GetInstructionSet(), type);
	    uintptr_t caller_pc = *reinterpret_cast<uintptr_t*>((reinterpret_cast<uint8_t*>(sp) + callee_return_pc_offset));
	    return caller_pc - size_of_instr;
	  }
	}
      }
    }
    return 1337;
  }

  // skip = 3 : "wrap_jni_XXX" "jni_XXX" "ProbeManager method_XXX"
  uint64_t GetEventAddrFromJni(unsigned int number_of_skip = 3)
      SHARED_REQUIRES(Locks::mutator_lock_) {
    // BacktraceMap* map = BacktraceMap::Create(getpid());
    // Backtrace* backtrace(Backtrace::Create(BACKTRACE_CURRENT_PROCESS, Thread::Current()->GetTid(), map));
    Backtrace* backtrace(Backtrace::Create(BACKTRACE_CURRENT_PROCESS, Thread::Current()->GetTid()));
    if(!backtrace->Unwind(number_of_skip + 2)) { // + 2 : GetEventAddrFromContext + Unwind
      GLOG(INFO, "(backtrace::Unwind failed for thread: " << backtrace->GetErrorString(backtrace->GetError()));
    } else if(backtrace->NumFrames() == 0) {
      GLOG(INFO, "(no native stack frames for thread ");
    } else {
      Backtrace::const_iterator it = backtrace->begin();
      if(it != backtrace->end())
	return it->pc;
    }
    return 1338;
  }

  void ProbeManager::DeliverException(mirror::Throwable* exception)
    SHARED_REQUIRES(Locks::mutator_lock_) {
    if(IsProcDebug()) {
      pthread_mutex_lock(&vector_mutex);
      DBG(DBG_VECTOR_MUTEX) GLOG(INFO, "DeliverException");
      // size_t index = Thread::Current()->GetIndexInThreadVector();
      // if(heap_activation_vector[index]->top() > 0) {
      
    	if(is_heap_disabled[Thread::Current()->GetIndexInThreadVector()])
    	  ForceHeapReenable(true);

    	mirror::Class* cls = exception->GetClass();
    	StackHandleScope<1> hs(Thread::Current());

    	event->set_tid(Thread::Current()->GetTid());
    	event->set_event_address(GetEventAddr());
    	throwEv->set_exception_type(cls->ComputeName(hs.NewHandle(cls))->ToModifiedUtf8());
    	if(exception->GetDetailMessage())
    	  throwEv->set_exception_text(exception->GetDetailMessage()->ToModifiedUtf8());
    	else
    	  throwEv->set_exception_text("");
    	event->set_allocated_throw_(throwEv);
    	LogEventToSocket(event);
    	event->release_throw_();

    	/* Release allocated buffer to avoid protobuf from deleting it */
    	event->release_throw_();

    	if(is_heap_disabled[Thread::Current()->GetIndexInThreadVector()])
	  // SwitchToAnalyzedMode();
    	  ForceHeapDisable(true);
      // }

      DBG(DBG_VECTOR_MUTEX) GLOG(INFO, "DeliverException end");
      pthread_mutex_unlock(&vector_mutex);
    }
  }

  void ProbeManager::CatchException(uint64_t dex_pc_or_asm_pc, unsigned int nb_skipped_frame)
    SHARED_REQUIRES(Locks::mutator_lock_) {
    if(IsProcDebug()) {
      // GLOG(INFO, "NB_SKIPPED_FRAME " << nb_skipped_frame);
      pthread_mutex_lock(&vector_mutex);
      DBG(DBG_VECTOR_MUTEX) GLOG(INFO, "CatchException");

      // size_t index = Thread::Current()->GetIndexInThreadVector();
      // if(heap_activation_vector[index]->top() > 0) {
    	event->set_tid(Thread::Current()->GetTid());
    	event->set_event_address(dex_pc_or_asm_pc);
    	catchEv->set_nb_skipped_frame(nb_skipped_frame);
    	event->set_allocated_catch_(catchEv);
    	LogEventToSocket(event);
    	event->release_catch_();

    	// ModifyHeapActivationVector( -nb_skipped_frame);
      // }

      DBG(DBG_VECTOR_MUTEX) GLOG(INFO, "CatchException end");
      pthread_mutex_unlock(&vector_mutex);
    }
  }

  void ProbeManager::MonitorAction(__attribute__((__unused__))bool enter, __attribute__((__unused__))mirror::Object* obj)
    SHARED_REQUIRES(Locks::mutator_lock_) {
    // if(IsProcDebug()) {
    //   pthread_mutex_lock(&vector_mutex);

    //   event->set_tid(Thread::Current()->GetTid());
    //   event->set_event_address(0);

    //   StackHandleScope<1> hs(Thread::Current());
    //   mirror::Class* cls = obj->GetClass();
    //   AllocateUntil(valueEv, 0);
    //   valueEv[0]->set_type(cls->ComputeName(hs.NewHandle(cls))->ToModifiedUtf8());
    //   valueEv[0]->set_ui64((unsigned long)obj);

    //   monitorEv->set_enter(enter);
    //   monitorEv->set_allocated_obj(valueEv[0]);
    //   event->set_allocated_monitor(monitorEv);

    //   LogEventToSocket(event);

    //   event->release_monitor();
    //   monitorEv->release_obj();

    //   pthread_mutex_unlock(&vector_mutex);
    // }
  }

  void ProbeManager::ObjectAllocated(mirror::Class* klass, void* allocated_addr)
    SHARED_REQUIRES(Locks::mutator_lock_) {
    if(IsProcDebug()) {
      std::string temp;

      pthread_mutex_lock(&vector_mutex);
      DBG(DBG_VECTOR_MUTEX) GLOG(INFO, "ObjectAllocated");
      
      if(is_heap_disabled[Thread::Current()->GetIndexInThreadVector()]) {
	ForceHeapReenable(true);
      }
      
      event->set_tid(Thread::Current()->GetTid());
      event->set_event_address(GetEventAddr());

      /* TODO: replace by a call to GetEventAddr */
      Runtime* runtime = Runtime::Current();
      Thread* thd = Thread::Current();
      ArtMethod** sp = thd->GetManagedStack()->GetTopQuickFrame();
      if(sp) {
	for (int i = 0; i < Runtime::kLastCalleeSaveType; i++) {
	  Runtime::CalleeSaveType type = Runtime::CalleeSaveType(i);
	  if (*sp == runtime->GetCalleeSaveMethod(type)) {
	    const size_t callee_return_pc_offset = GetCalleeSaveReturnPcOffset(runtime->GetInstructionSet(), type);
	    uintptr_t caller_pc = *reinterpret_cast<uintptr_t*>((reinterpret_cast<uint8_t*>(sp) + callee_return_pc_offset));
	    event->set_event_address(caller_pc);
	  }
	}
      }

      AllocateUntil(valueEv, 0);
      valueEv[0]->set_type(klass->GetDescriptor(&temp));
      valueEv[0]->set_ui64((unsigned long)allocated_addr);
      
      newEv->set_allocated_allocated_value(valueEv[0]);
      
      event->set_allocated_newobj(newEv);
      LogEventToSocket(event);
      
      newEv->release_allocated_value();
      event->release_newobj();

      if(is_heap_disabled[Thread::Current()->GetIndexInThreadVector()]) {
	// SwitchToAnalyzedMode();
	ForceHeapDisable(true);
      }

      DBG(DBG_VECTOR_MUTEX) GLOG(INFO, "ObjectAllocated end");
      pthread_mutex_unlock(&vector_mutex);
    }
  }

  bool ProbeManager::MethodInvoked(ArtMethod* method, uint32_t* args, uint32_t args_size) {
    if(IsProcDebug()) {

      pthread_mutex_lock(&vector_mutex);
      DBG(DBG_VECTOR_MUTEX) GLOG(INFO, "MethodInvoked 1");

      if(is_heap_disabled[Thread::Current()->GetIndexInThreadVector()]) {
	ForceHeapReenable(true);
      }
	

      uint8_t non_static_method = method->IsStatic()?0:1;
      const DexFile::TypeList* typeList = method->GetParameterTypeList();
      if(args_size - non_static_method <= 0 || typeList) {

	unsigned int dumpId = 0;
	if(IsProcDump())
	  if(IsDumpedMethod(method))
	    dumpId = Runtime::Current()->GetSignalManager()->DumpProccessToFile(is_heap_disabled[Thread::Current()->GetIndexInThreadVector()]);

	event->set_tid(Thread::Current()->GetTid());
	event->set_event_address(GetEventAddr());

	// if(method) {
	//   GLOG(INFO, "[INV_1] " << method->GetDeclaringClassDescriptor() << method->GetName() << " " << method->GetSignature().ToString() << (method->IsStatic()?0:1));
	// }

	AllocateUntil(valueEv, args_size+1); // valueEv[0] reserved for caller_value
	::google::protobuf::RepeatedPtrField< ::protobuf_log::Event_Value >* argsEv = invokeEv->mutable_args();

	for(unsigned int i=0 ; i < args_size - non_static_method; i++) {
	  const char* descriptor;
	  if(typeList)
	    descriptor = method->GetTypeDescriptorFromTypeIdx(typeList->GetTypeItem(i).type_idx_);
	  else {
	    descriptor = "?";
	  }
	  FillProtoValue(args[i + non_static_method], descriptor, valueEv[i + 1]);
	  argsEv->AddAllocated(valueEv[i + 1]);
	}

	valueEv[0]->set_type(method->GetDeclaringClassDescriptor());
	valueEv[0]->set_i32(args_size ? args[0] : 0);
	invokeEv->set_allocated_caller_value(valueEv[0]);
	invokeEv->set_method_name(method->GetName());
	invokeEv->set_dump_id(dumpId);
	event->set_allocated_invoke(invokeEv);
	LogEventToSocket(event);

	for(unsigned int i=0 ; i < args_size - non_static_method ; i++)
	  argsEv->ReleaseLast();
	invokeEv->release_caller_value();
	event->release_invoke();

	if(IsAudited(method)) {
	  GLOG(INFO, "start counting !");
	  time_to_count = true;
	}
	
	if((method->IsNative() || Runtime::Current()->GetClassLinker()->GetOatMethodQuickCodeFor(method) != 0) && !IsWhiteListedMethod(method))
	  ModifyHeapActivationVector(1);
	else
	  UntrackEnter();
	
	DBG(DBG_VECTOR_MUTEX) GLOG(INFO, "MethodInvoked 1 end");
	pthread_mutex_unlock(&vector_mutex);

	return true;
      } else {
	GLOG(INFO, "ERROR: args_size - non_static_method < 0 and typeList is null");
	GLOG(INFO, "\t" << method->GetDeclaringClassDescriptor() << method->GetName() << " " << method->GetSignature().ToString());
      }

      if(is_heap_disabled[Thread::Current()->GetIndexInThreadVector()]) {
	// SwitchToAnalyzedMode();
	ForceHeapDisable(true);
      }
	
      DBG(DBG_VECTOR_MUTEX) GLOG(INFO, "MethodInvoked 1 end (bis)");
      pthread_mutex_unlock(&vector_mutex);
    }
    return false;
  }

#if defined(__aarch64__)
  void ProbeManager::MethodInvoked(ArtMethod* method, const uint8_t* begin_, uint32_t code_offset_, ucontext_t* u_ctx) {
#elif defined(__arm__)
  void ProbeManager::MethodInvoked(ArtMethod* method, __attribute__((__unused__)) const uint8_t* begin_, __attribute__((__unused__)) uint32_t code_offset_, ucontext_t* u_ctx) {
#else
  void ProbeManager::MethodInvoked(ArtMethod* method, __attribute__((__unused__)) const uint8_t* begin_, __attribute__((__unused__))  uint32_t code_offset_, __attribute__((__unused__)) ucontext_t* u_ctx) {
#endif
    pthread_mutex_lock(&vector_mutex);
    DBG(DBG_VECTOR_MUTEX) GLOG(INFO, "MethodInvoked 2 ");

    if(is_heap_disabled[Thread::Current()->GetIndexInThreadVector()]) {
      ForceHeapReenable(true);
    }

    unsigned int dumpId = 0;
    if(IsProcDump())
      if(IsDumpedMethod(method))
    	dumpId = Runtime::Current()->GetSignalManager()->DumpProccessToFile(is_heap_disabled[Thread::Current()->GetIndexInThreadVector()]);

    event->set_tid(Thread::Current()->GetTid());
#if defined(__arm__)
    event->set_event_address(u_ctx->uc_mcontext.arm_lr-4);
#elif defined(__aarch64__)
    // event->set_event_address(u_ctx->uc_mcontext.regs[30]-4);
    // event->set_event_address(0x22222222);

    uint32_t dexpc = GetDexPcInVector(Thread::Current());
    //if(dexpc != 0xffffffff)
      event->set_event_address(dexpc);
      //else
      //event->set_event_address(u_ctx->uc_mcontext.regs[30]-4);
      //GLOG(INFO, "DexPc = " << dexpc);
      //SetDexPcInVector(0xeeeeeeee, Thread::Current());

    // if(method) {
    //   GLOG(INFO, "[INV_2] " << method->GetDeclaringClassDescriptor() << method->GetName());
    // }

    // if(Thread* thd = Thread::Current()) {
    //   for(const ManagedStack* ms = thd->GetManagedStack() ; ms != nullptr ; ms = ms->GetLink()) {	     	// if((ms = ms->GetLink())) {
    	//   if((ms = ms->GetLink())) {
	    // ArtMethod** am = ms->GetTopQuickFrame();
	    // if(am != nullptr) {
	    //   if(*am != nullptr) {
	    // 	if(strncmp("Ljava/util/", method->GetDeclaringClassDescriptor(), 11) != 0) {
		  // GLOG(INFO, "\taddr: " << (void*) *am);
		  // const DexFile* dex_file = (*am)->GetDexFile();
		  // GLOG(INFO, "\tdex_file " << (void*) dex_file);
	    // 	  GLOG(INFO, "\t" << (*am)->GetName());
	    // 	}
	    //   }
	    // }
    	    // if(ShadowFrame* sf = ms->GetTopShadowFrame()) {
    	    //   event->set_event_address(sf->GetDexPC());
    	    //   GLOG(INFO, "[INV] DexPC = " << sf->GetDexPC());
    	    // }
    	//   }
    	// }
    //   }
    // }

    // // void** fp = (void**)(u_ctx->uc_mcontext.regs[29] & (~(unsigned long long)7) ); // x29 = fp
    // void** fp = (void**) u_ctx->uc_mcontext.regs[29]; // x29 = fp
    // if((unsigned long)fp > 0x1000) {
    //   GLOG(INFO, "fp = " << fp);
    //   void* lr_prim = fp[1];
    //   event->set_event_address((unsigned long)lr_prim);
    // } else
    //   event->set_event_address(0);
    
    // // Show instrs at lr - 4
    // unsigned char * b = (unsigned char*)(u_ctx->uc_mcontext.regs[30]-4);
    // GLOG(INFO, "Bytes at ret: [" << static_cast<int>(b[0]) << ", " << static_cast<int>(b[1]) << ", " << static_cast<int>(b[2]) << ", " << static_cast<int>(b[3]) << ", " << static_cast<int>(b[4]) << ", " << static_cast<int>(b[5]) << ", " << static_cast<int>(b[6]) << ", " << static_cast<int>(b[7]) << ", " << static_cast<int>(b[8]) << ", " << static_cast<int>(b[9]) << ", " << static_cast<int>(b[10]) << ", " << static_cast<int>(b[11]) << ", " << static_cast<int>(b[12]) << ", " << static_cast<int>(b[13]) << ", " << static_cast<int>(b[14]) << ", " << static_cast<int>(b[15]) << ", " << static_cast<int>(b[16]) << ", " << static_cast<int>(b[17]) << ", " << static_cast<int>(b[18]) << ", " << static_cast<int>(b[19]) << "]");
#else
    event->set_event_address(0);
#endif

    uint32_t nb_arg = method->GetParameterTypeList() ? method->GetParameterTypeList()->Size() : 0;

    AllocateUntil(valueEv, nb_arg); // valueEv[0] reserved for caller_value

#if defined(__arm__) || defined(__aarch64__)
    ::google::protobuf::RepeatedPtrField< ::protobuf_log::Event_Value >* args = invokeEv->mutable_args();
#endif

#if defined(__arm__)
    /* See quick invocation stuc from
     * art/runtime/arch/arm/quick_entrypoints_arm64.S */
    for(unsigned int i=0 ; i < nb_arg ; i++) {
      const char* descriptor = method->GetTypeDescriptorFromTypeIdx(i);

      /* TODO: fill the value */
      FillProtoValue((unsigned long)0, descriptor, valueEv[i+1]);

      args->AddAllocated(valueEv[i+1]);
    }
#elif defined(__aarch64__)
    // bool found;

    // OatFile::OatMethod oat_method = Runtime::Current()->GetClassLinker()->FindOatMethodFor(method, &found);
    OatFile::OatMethod oat_method = OatFile::OatMethod(begin_, code_offset_);

    // if(found) {
      size_t frame_size = oat_method.GetFrameSizeInBytes();
      
      /* Algo from "Procedure Call Standard for the ARM 64-bit Architecture",
       * chapter 5.3, adapted using art_quick_invoke_stub description in
       * art/runtime/arch/arm64/quick_entrypoints_arm64.S */
      unsigned int NGRN = method->IsStatic() ? 1 : 2; // Next General-purpose Register Number
      unsigned int NSRN = 0; // Next SIMD and Floating point Register Number
      void* NSAA = (void*) (u_ctx->uc_mcontext.sp + frame_size + InstructionSetPointerSize(kArm64)); // Next Stacked Argument Address
      
      struct fpsimd_context* fpsimd_ctx = SignalManager::GetFpsimdFromContext(u_ctx);
      const DexFile::TypeList* typeList = method->GetParameterTypeList();

      for(unsigned int i=0 ; i < nb_arg ; i++) {
	const char* descriptor = method->GetTypeDescriptorFromTypeIdx(typeList->GetTypeItem(i).type_idx_);

	unsigned long concreate_value = 0;
	switch(descriptor[0]) {
	case 'D': // dNSRN
	  NSAA = (void*)((char*)NSAA + 8);
	  if(NSRN < 8)
	    concreate_value = fpsimd_ctx->vregs[NSRN++];
	  else {
	    concreate_value = *(unsigned long*) NSAA;
	  }
	  break;
	case 'F': // sNSRN
	  NSAA = (void*)((char*)NSAA + 4);
	  if(NSRN < 8)
	    concreate_value = fpsimd_ctx->vregs[NSRN++] & 0xffffffff;
	  else {
	    concreate_value = *(unsigned int*) NSAA;
	  }
	  break;
	case 'J': // xNGRN
	  NSAA = (void*)((char*)NSAA + 8);
	  if(NGRN < 8)
	    concreate_value = u_ctx->uc_mcontext.regs[NGRN++];
	  else {
	    concreate_value = *(unsigned long*) NSAA;
	  }
	  break;
	case 'I':
	case 'S':
	case 'Z':
	case 'B':
	case 'C':
	case 'L':
	case '[': // wNGRN
	  NSAA = (void*)((char*)NSAA + 4);
	  if(NGRN < 8)
	    concreate_value = u_ctx->uc_mcontext.regs[NGRN++] & 0xffffffff;
	  else {
	    concreate_value = *(unsigned int*) NSAA;
	  }
	  break;
	}
	FillProtoValue(concreate_value, descriptor, valueEv[i+1]);

	args->AddAllocated(valueEv[i+1]);
      }
    // } else {
    //   // GLOG(INFO, "OAT file not found!");
    // }
#endif

#if defined(__arm__)
      if(!method->IsStatic())
	FillProtoValue(u_ctx->uc_mcontext.arm_r1, method->GetDeclaringClassDescriptor(), valueEv[0]);
      else
	FillProtoValue((unsigned long)0, method->GetDeclaringClassDescriptor(), valueEv[0]);
#elif defined(__aarch64__)
      if(!method->IsStatic())
	FillProtoValue(u_ctx->uc_mcontext.regs[1], method->GetDeclaringClassDescriptor(), valueEv[0]);
      else
	FillProtoValue((unsigned long)0, method->GetDeclaringClassDescriptor(), valueEv[0]);
#else
    valueEv[0]->set_type(method->GetDeclaringClassDescriptor());
    valueEv[0]->set_i32(0);
#endif

    invokeEv->set_allocated_caller_value(valueEv[0]);
    invokeEv->set_method_name(method->GetName());
    invokeEv->set_dump_id(dumpId);
    event->set_allocated_invoke(invokeEv);
    LogEventToSocket(event);

    /* Release allocated buffer to avoid protobuf from deleting it */
#if defined(__arm__) || defined(__aarch64__)
    for(unsigned int i=0 ; i < nb_arg ; i++)
      args->ReleaseLast();
#endif
    invokeEv->release_caller_value();
    event->release_invoke();

    if(IsAudited(method)) {
      GLOG(INFO, "start counting !");
      time_to_count = true;
    }
    
    if(!IsWhiteListedMethod(method))
      ModifyHeapActivationVector(1);
    else
      UntrackEnter();

    if(is_heap_disabled[Thread::Current()->GetIndexInThreadVector()]) {
      // SwitchToAnalyzedMode();
      ForceHeapDisable(true);
    }

    DBG(DBG_VECTOR_MUTEX) GLOG(INFO, "MethodInvoked 2 end");
    pthread_mutex_unlock(&vector_mutex);
  }

  bool ProbeManager::MethodInvoked(ShadowFrame& shadow_frame, const DexFile::CodeItem* code_item) {
    if(IsProcDebug()) {
      // GLOG(INFO, "MethodInvoked 3");
      ArtMethod *method = shadow_frame.GetMethod();
      uint16_t arg_offset = (code_item == nullptr)
                            ? 0
                            : code_item->registers_size_ - code_item->ins_size_;
      uint32_t* args = shadow_frame.GetVRegArgs(arg_offset);
      uint32_t args_size = shadow_frame.NumberOfVRegs() - arg_offset;
      return MethodInvoked(method, args, args_size);
    }

    return 0;
  }

  void ProbeManager::MethodReturned(ArtMethod* method, JValue* result) {
    if(IsProcDebug()) {
      pthread_mutex_lock(&vector_mutex);
      DBG(DBG_VECTOR_MUTEX) GLOG(INFO, "MethodReturned 1");
      if(is_heap_disabled[Thread::Current()->GetIndexInThreadVector()])
	ForceHeapReenable(true);

      if(IsAudited(method)) {
	time_to_count = false;
	GLOG(INFO, "SEGV: " << nb_segv);
	GLOG(INFO, "TRAP: " << nb_trap);
	GLOG(INFO, "READ: " << nb_read);
	GLOG(INFO, "WRITE: " << nb_write);
	GLOG(INFO, "INVOKE: " << nb_invoke);
	GLOG(INFO, "RET: " << nb_ret);
	GLOG(INFO, "NEW: " << nb_new);
	GLOG(INFO, "THROW: " << nb_throw);
	GLOG(INFO, "CATCH: " << nb_catch);
	GLOG(INFO, "LOAD: " << nb_load);
	GLOG(INFO, "PROTOBUF: " << time_protobuf);
	time_protobuf=0;
	nb_segv = 0;
	nb_trap = 0;
	nb_read = 0;
	nb_write = 0;
	nb_invoke = 0;
	nb_ret = 0;
	nb_new = 0;
	nb_throw = 0;
	nb_catch = 0;
	nb_load = 0;
      }

      if(method) {
	// GLOG(INFO, "[RET_1] " << method->GetDeclaringClassDescriptor() << method->GetName());
      }
      
      AllocateUntil(valueEv, 0);
      event->set_tid(Thread::Current()->GetTid());
      event->set_event_address(GetEventAddr());
      const char* descriptor = method->GetReturnTypeDescriptor();
      FillProtoValue((void*)result, descriptor, valueEv[0]);
      retEv->set_allocated_ret_value(valueEv[0]);
      event->set_allocated_ret(retEv);
      LogEventToSocket(event);

      retEv->release_ret_value();
      event->release_ret();

      if(method->IsNative() && !IsWhiteListedMethod(method))
	  ModifyHeapActivationVector(-1);
      else
	UntrackExit();

      if(is_heap_disabled[Thread::Current()->GetIndexInThreadVector()])
	// SwitchToAnalyzedMode();
	ForceHeapDisable(true);
      
      DBG(DBG_VECTOR_MUTEX) GLOG(INFO, "MethodReturned 1 end");
      pthread_mutex_unlock(&vector_mutex);
    }
  }

#if defined(__arm__) || defined(__aarch64__)
  void ProbeManager::MethodReturned(ArtMethod* method, ucontext_t* u_ctx) {
#else
  void ProbeManager::MethodReturned(ArtMethod* method, __attribute__((__unused__)) ucontext_t* u_ctx) {
#endif
    pthread_mutex_lock(&vector_mutex);
    DBG(DBG_VECTOR_MUTEX) GLOG(INFO, "MethodReturned 2");

    if(is_heap_disabled[Thread::Current()->GetIndexInThreadVector()])
      ForceHeapReenable(true);

    if(IsAudited(method)) {
      time_to_count = false;
      GLOG(INFO, "SEGV: " << nb_segv);
      GLOG(INFO, "TRAP: " << nb_trap);
      GLOG(INFO, "READ: " << nb_read);
      GLOG(INFO, "WRITE: " << nb_write);
      GLOG(INFO, "INVOKE: " << nb_invoke);
      GLOG(INFO, "RET: " << nb_ret);
      GLOG(INFO, "NEW: " << nb_new);
      GLOG(INFO, "THROW: " << nb_throw);
      GLOG(INFO, "CATCH: " << nb_catch);
      GLOG(INFO, "LOAD: " << nb_load);
      GLOG(INFO, "PROTOBUF: " << time_protobuf);
      time_protobuf=0;
      nb_segv = 0;
      nb_trap = 0;
      nb_read = 0;
      nb_write = 0;
      nb_invoke = 0;
      nb_ret = 0;
      nb_new = 0;
      nb_throw = 0;
      nb_catch = 0;
      nb_load = 0;
    }

    AllocateUntil(valueEv, 0);

    event->set_tid(Thread::Current()->GetTid());
#if defined(__arm__)
    event->set_event_address(u_ctx->uc_mcontext.arm_pc);
#elif defined(__aarch64__)
    event->set_event_address(0);
    // event->set_event_address(u_ctx->uc_mcontext.pc);
    // event->set_event_address(GetEventAddr());
    // event->set_event_address(0x11111111);

    // //PopDexPcInVector(Thread::Current());
    // // Put RET just after INVOKE
    // // Instruction are 2 bytes long so it can not overlap an other
    // uint32_t dexpc = GetDexPcInVector(Thread::Current()+1);
    // //if(dexpc != 0xffffffff)
    //   event->set_event_address(dexpc);
    //   //else
    //   //event->set_event_address(u_ctx->uc_mcontext.pc);

    if(method) {
      // GLOG(INFO, "[RET_2] " << method->GetDeclaringClassDescriptor() << method->GetName());
    }
    
    // if(Thread* thd = Thread::Current()) {
    //   if(const ManagedStack* ms = thd->GetManagedStack()) {
    // 	// if((ms = ms->GetLink())) {
    // 	  // if((ms = ms->GetLink())) {
    // 	    ArtMethod** am = ms->GetTopQuickFrame();
    // 	    if(am && *am) {
    // 	      GLOG(INFO, "[RET] TopFrame = " << (*am)->GetName());
    // 	    }
    // 	    if(method) {
    // 	      GLOG(INFO, "[RET] Method = " << method->GetName());
    // 	    }
    // 	    if(ShadowFrame* sf = ms->GetTopShadowFrame()) {
    // 	      event->set_event_address(sf->GetDexPC());
    // 	      GLOG(INFO, "[RET] DexPC = " << sf->GetDexPC());
    // 	    }
    	  // }
    	// }
    //   }
    // }
    
    // Dl_info dl_info;
    // if(dladdr((void*)u_ctx->uc_mcontext.pc, &dl_info) != 0) {
    //   GLOG(INFO, "Name: " << dl_info.dli_fname << " " << method->GetDeclaringClassDescriptor() << " " << method->GetName() << " " << dl_info.dli_sname << " " << (void*)u_ctx->uc_mcontext.pc << " " << dl_info.dli_fbase);
    // }

    // GLOG(INFO, "[" << (void*)u_ctx->uc_mcontext.pc << "] ThreadState " << Thread::Current()->GetState());
    // Thread* thd = Thread::Current();
    // if(thd && thd->GetManagedStack()) {
    //   ArtMethod** sp = thd->GetManagedStack()->GetTopQuickFrame();
    //   if(sp && *sp) {
    // 	GLOG(INFO, method->GetName() << " " << (*sp)->GetName() << " ");
    // 	if(thd->GetManagedStack()->GetLink()) {
    // 	  ArtMethod** sp2 = thd->GetManagedStack()->GetLink()->GetTopQuickFrame();
    // 	    if(sp2 && *sp2) {
    // 	      GLOG(INFO, "\t" << (*sp2)->GetName());
    // 	    }
    // 	}
    //   }
    // }    
				  
    // Show instrs at pc
    // unsigned char * b = (unsigned char*) u_ctx->uc_mcontext.pc;
    // GLOG(INFO, "Bytes at ret: [" << static_cast<int>(b[0]) << ", " << static_cast<int>(b[1]) << ", " << static_cast<int>(b[2]) << ", " << static_cast<int>(b[3]) << ", " << static_cast<int>(b[4]) << ", " << static_cast<int>(b[5]) << ", " << static_cast<int>(b[6]) << ", " << static_cast<int>(b[7]) << ", " << static_cast<int>(b[8]) << ", " << static_cast<int>(b[9]) << ", " << static_cast<int>(b[10]) << ", " << static_cast<int>(b[11]) << ", " << static_cast<int>(b[12]) << ", " << static_cast<int>(b[13]) << ", " << static_cast<int>(b[14]) << ", " << static_cast<int>(b[15]) << ", " << static_cast<int>(b[16]) << ", " << static_cast<int>(b[17]) << ", " << static_cast<int>(b[18]) << ", " << static_cast<int>(b[19]) << "]");
#else
    event->set_event_address(0);
#endif
#if defined(__arm__)
    // TODO: float values
    FillProtoValue(u_ctx->uc_mcontext.arm_r0, method->GetReturnTypeDescriptor(), valueEv[0]);
#elif defined(__aarch64__)
    const char* descriptor = method->GetReturnTypeDescriptor();
    struct fpsimd_context* fpsimd_ctx = SignalManager::GetFpsimdFromContext(u_ctx);
    switch(descriptor[0]) {
    case 'D':
      FillProtoValue(fpsimd_ctx->vregs[0], descriptor, valueEv[0]);
      break;
    case 'F':
      FillProtoValue(fpsimd_ctx->vregs[0] & 0xffffffff, descriptor, valueEv[0]);
      break;
    case 'J':
    case 'I':
    case 'S':
    case 'Z':
    case 'B':
    case 'C':
    case 'L':
    case '[':
      FillProtoValue(u_ctx->uc_mcontext.regs[0], descriptor, valueEv[0]);
      break;
    case 'V':
      valueEv[0]->set_type(descriptor);
      valueEv[0]->set_i64(0);
    }
#else
    valueEv[0]->set_type(method->GetReturnTypeDescriptor());
    valueEv[0]->set_i64(0);
#endif
    retEv->set_allocated_ret_value(valueEv[0]);
    event->set_allocated_ret(retEv);
    LogEventToSocket(event);

    retEv->release_ret_value();
    event->release_ret();

    if(!IsWhiteListedMethod(method))
      ModifyHeapActivationVector(-1);
    else
      UntrackEnter();

    if(is_heap_disabled[Thread::Current()->GetIndexInThreadVector()])
      // SwitchToAnalyzedMode();
      ForceHeapDisable(true);

    DBG(DBG_VECTOR_MUTEX) GLOG(INFO, "MethodReturned 2 end");
    pthread_mutex_unlock(&vector_mutex);
  }

  void ProbeManager::MethodReturned(ShadowFrame& shadow_frame, JValue result) {
    // GLOG(INFO, "MethodReturned 3");
    ArtMethod *method = shadow_frame.GetMethod();
    MethodReturned(method, &result);
    // GLOG(INFO, "MethodReturned 3 end");
  }

  
  void ProbeManager::SetDexPcInVector(uint32_t dexpc, Thread* self) {
    if(!self) {
      self = Thread::Current();
    }
    // GLOG(INFO, "DEX_PC_VECTOR PUSH " << dexpc);
    dex_pc_vector[self->GetIndexInThreadVector()]->push(dexpc);
  }

  uint32_t ProbeManager::GetDexPcInVector(Thread* self) {
    if(!self) {
      self = Thread::Current();
    }
    if(dex_pc_vector[self->GetIndexInThreadVector()]->size() > 0)
      return dex_pc_vector[self->GetIndexInThreadVector()]->top();
    else
      return 0xffffffff;
  }

  void ProbeManager::PopDexPcInVector(Thread* self) {
    if(!self) {
      self = Thread::Current();
    }
    if(dex_pc_vector[self->GetIndexInThreadVector()]->size() > 0)
      dex_pc_vector[self->GetIndexInThreadVector()]->pop();
    // GLOG(INFO, "DEX_PC_VECTOR POP");
  }
  
  void ProbeManager::NewThread(Thread* current) {
#if defined(__aarch64__)
    Runtime::Current()->GetSignalManager()->last_exclusive_addr_vector.push_back(0);
#endif

    pthread_mutex_lock(&vector_mutex);
    DBG(DBG_VECTOR_MUTEX) GLOG(INFO, "NewThread");

    std::stack<int>* st = new std::stack<int>();
    st->push(0);
    heap_activation_vector.push_back(st);
    std::stack<uint32_t>* st2 = new std::stack<uint32_t>();
    dex_pc_vector.push_back(st2);
    is_heap_disabled.push_back(false);
    current->SetIndexInThreadVector(heap_activation_vector.size()-1);

    std::stack<void*>* lr_st = new std::stack<void*>();
    Runtime::Current()->GetSignalManager()->wrapper_lr.push_back(lr_st);

    DBG(DBG_VECTOR_MUTEX) GLOG(INFO, "NewThread end");
    pthread_mutex_unlock(&vector_mutex);

    sigset_t sigset;
    sigprocmask(SIG_SETMASK, NULL, &sigset);
    sigdelset(&sigset, SIGABRT);
    sigprocmask(SIG_SETMASK, &sigset, NULL);
  }

  void ProbeManager::GcTriggered() {
    pthread_mutex_lock(&vector_mutex);
    DBG(DBG_VECTOR_MUTEX) GLOG(INFO, "GcTriggered");
    touched_field_cache.flush();
    DBG(DBG_VECTOR_MUTEX) GLOG(INFO, "GcTriggered END");
    pthread_mutex_unlock(&vector_mutex);
  }

  // We don't need mutex in these functions because there are called
  // when all other threads are paused
  // vector_mutex NEED to be held before calling this method
  void ProbeManager::BeforeTouched(void* instr_addr, void* touched_addr_) {
    // GLOG(INFO, "BeforeTouched");
    size_t index = Thread::Current()->GetIndexInThreadVector();
    if(heap_activation_vector[index]->top() > 0) {
      std::string temp;
      touched_addr = touched_addr_;
      touched_class = NULL;
      touched_object = NULL;
      touched_field = NULL;

      std::tuple<mirror::Object*, mirror::Class*, ArtField*>* entry;
      entry = touched_field_cache.lookup(touched_addr);
      if(entry == NULL) {
	// TODO: very slow method, should be reimplemented for our test case
	// TODO: we also should create a cache which is invalidated by GC triggering
	// const clock_t begin_time = clock();
	// Runtime::Current()->GetHeap()->VisitObjectsInternal(visitCallback, touched_addr);
	gc::Heap* heap = Runtime::Current()->GetHeap();

	/* Copy from Heap::VisitObjectsInternal */
	if (heap->bump_pointer_space_ != nullptr) {
	  // Visit objects in bump pointer space.
	  heap->bump_pointer_space_->Walk(visitCallback, touched_addr);
	}
	// TODO: Switch to standard begin and end to use ranged a based loop.
	for (auto* it = heap->allocation_stack_->Begin(), *end = heap->allocation_stack_->End(); it < end; ++it) {
	  mirror::Object* const obj = it->AsMirrorPtr();
	  if (obj != nullptr && obj->GetClass() != nullptr) {
	    // Avoid the race condition caused by the object not yet being written into the allocation
	    // stack or the class not yet being written in the object. Or, if
	    // kUseThreadLocalAllocationStack, there can be nulls on the allocation stack.
	    visitCallback(obj, touched_addr);
	  }
	}
	// usleep(8);
	// Log grosoo mode 0.008 = 8ms
	// GLOG(INFO, "time " <<  float( clock () - begin_time ) /  CLOCKS_PER_SEC);

	if(touched_object != nullptr)
	  touched_field_cache.add_entry(touched_addr, std::make_tuple(touched_object, touched_class, touched_field));
      }
      else {
	touched_object = std::get<0>(*entry);
	touched_class = std::get<1>(*entry);
	touched_field = std::get<2>(*entry);
      }
      
      if(touched_object != NULL) {
	const char* class_descriptor = touched_class->GetDescriptor(&temp);

	event->set_tid(Thread::Current()->GetTid());
	event->set_event_address((unsigned long long)instr_addr);
	AllocateUntil(valueEv, 2); // caller, first value, new value if any

	FillProtoValue((void*)touched_object, class_descriptor, valueEv[0]);
	
	// Get, if any, the touched field value
	if(touched_field) {
	  if(touched_field->FieldSize()) {
	    switch(touched_field->FieldSize()) {
	    case 1:
	      touched_value.b8 = *(uint8_t*) touched_addr;
	      break;
	    case 2:
	      touched_value.b16 = *(uint16_t*) touched_addr;
	      break;
	    case 4:
	      touched_value.b32 = *(uint32_t*) touched_addr;
	      break;
	    case 8:
	      touched_value.b64 = *(uint64_t*) touched_addr;
	      break;
	    }
	  }
	  FillProtoValue(touched_addr, touched_field->GetTypeDescriptor(), valueEv[1]);
	} else {
	  if(class_descriptor[0] == '[') {
	    FillProtoValue(touched_addr, class_descriptor+1, valueEv[1]);

	    switch(class_descriptor[1]) {
	    case 'D':
	    case 'J':
	    case 'L':
	      touched_value.b64 = *(uint64_t*) touched_addr;
	      break;
	    case 'F':
	    case 'I':
	    case 'S':
	      touched_value.b32 = *(uint32_t*) touched_addr;
	      break;
	    case 'Z': /* TODO: Verify bool is 8 bits */
	    case 'B':
	    case 'C':
	      touched_value.b8 = *(uint8_t*) touched_addr;
	      break;
	    }
	  } else {
	    // GLOG(INFO, "\tRead offset doesn't correspond to any field: " << ((unsigned char*)touched_addr) - ((unsigned char*)touched_object) << ", class descriptor: " << class_descriptor);
	  }
	}
      } else {
	// GLOG(INFO, "Object not found! " << touched_addr_ << " @" << instr_addr);
      }
    }
    // GLOG(INFO, "BeforeTouched end");
  }

  // vector_mutex NEED to be held before calling this method
  void ProbeManager::AfterTouched() {
    // GLOG(INFO, "AfterTouched");
    size_t index = Thread::Current()->GetIndexInThreadVector();
    if(heap_activation_vector[index]->top() > 0) {
      bool value_writen = false;
      // Get, if any, the new touched field value
      if(touched_object != NULL) {
	if(heap_activation_vector[Thread::Current()->GetIndexInThreadVector()]->top() > 0) {
	  if(touched_field) {
	    if(touched_field->FieldSize()) {
	      switch(touched_field->FieldSize()) {
	      case 1:
		if(touched_value.b8 != *(uint8_t*) touched_addr)
		  value_writen = true;
		break;
	      case 2:
		if(touched_value.b16 != *(uint16_t*) touched_addr)
		  value_writen = true;
		break;
	      case 4:
		if(touched_value.b32 != *(uint32_t*) touched_addr)
		  value_writen = true;
		break;
	      case 8:
		if(touched_value.b64 != *(uint64_t*) touched_addr)
		  value_writen = true;
		break;
	      }
	    }

	    // const char* descriptor = touched_field->GetTypeDescriptor();
	    if(value_writen) {
	      // if(descriptor[0] != 'Z' && (descriptor[0] != '[' || descriptor[1] != 'Z')) {
	      FillProtoValue((void*)touched_addr, touched_field->GetTypeDescriptor(), valueEv[2]);
	      
	      writeEv->set_allocated_caller_value(valueEv[0]);
	      writeEv->set_field_name(touched_field->GetName());
	      writeEv->set_allocated_old_value(valueEv[1]);
	      writeEv->set_allocated_new_value(valueEv[2]);
	      event->set_allocated_write(writeEv);

	      // GLOG(INFO, "Log write " << touched_field->GetName() << " " << descriptor);
	      LogEventToSocket(event);

	      writeEv->release_caller_value();
	      writeEv->release_old_value();
	      writeEv->release_new_value();
	      event->release_write();
	      // } else {
	      //   GLOG(INFO, "Skiped write bool");
	      // }
	    } else {
	      // if(descriptor[0] != 'Z' && (descriptor[0] != '[' || descriptor[1] != 'Z')) {
	      readEv->set_allocated_caller_value(valueEv[0]);
	      readEv->set_field_name(touched_field->GetName());
	      readEv->set_allocated_field_value(valueEv[1]);
	      event->set_allocated_read(readEv);

	      // GLOG(INFO, "log read " << touched_field->GetName() << " " << descriptor);
	      LogEventToSocket(event);

	      readEv->release_caller_value();
	      readEv->release_field_value();
	      event->release_read();
	      // } else {
	      //   GLOG(INFO, "Skiped read bool");
	      // }
	    }
	  }
	  else {
	    std::string temp;
	    const char* class_descriptor = touched_class->GetDescriptor(&temp);
	    unsigned short component_size = 1;
	    if(class_descriptor[0] == '[') {
	      switch(class_descriptor[1]) {
	      case 'D':
	      case 'J':
	      case 'L':
		component_size = 8;
		if(touched_value.b64 != *(uint64_t*) touched_addr)
		  value_writen = true;
		break;
	      case 'F':
	      case 'I':
	      case 'S':
		component_size = 4;
		if(touched_value.b32 != *(uint32_t*) touched_addr)
		  value_writen = true;
		break;
	      case 'Z': /* TODO: Verify bool is 8 bits */
	      case 'B':
	      case 'C':
		component_size = 1;
		if(touched_value.b8 != *(uint8_t*) touched_addr)
		  value_writen = true;
		break;
	      }

	      char touched_field_name[32];
	      int offset = (unsigned char*)touched_addr - (unsigned char*)touched_object;
	      if(offset == ((mirror::Array*)touched_object)->LengthOffset().Int32Value())
		snprintf(touched_field_name, sizeof(touched_field_name), "length");
	      else {
		offset -= ((mirror::Array*)touched_object)->DataOffset(component_size).Int32Value();
		snprintf(touched_field_name, sizeof(touched_field_name), "elems[%i]", offset/(component_size*2));
	      }

	      if(value_writen) {
		if(class_descriptor[0] != 'Z' && (class_descriptor[0] != '[' || class_descriptor[1] != 'Z')) {
		  FillProtoValue((void*)touched_addr, class_descriptor+1, valueEv[2]);

		  writeEv->set_allocated_caller_value(valueEv[0]);
		  writeEv->set_field_name(touched_field_name);
		  writeEv->set_allocated_old_value(valueEv[1]);
		  writeEv->set_allocated_new_value(valueEv[2]);
		  event->set_allocated_write(writeEv);

		  LogEventToSocket(event);

		  writeEv->release_caller_value();
		  writeEv->release_old_value();
		  writeEv->release_new_value();
		  event->release_write();
		} else {
		  GLOG(INFO, "Skiped write bool");
		}
	      } else {
		if(class_descriptor[0] != 'Z' && (class_descriptor[0] != '[' || class_descriptor[1] != 'Z')) {
		  readEv->set_allocated_caller_value(valueEv[0]);
		  readEv->set_field_name(touched_field_name);
		  readEv->set_allocated_field_value(valueEv[1]);
		  event->set_allocated_read(readEv);

		  LogEventToSocket(event);

		  readEv->release_caller_value();
		  readEv->release_field_value();
		  event->release_read();
		} else {
		  GLOG(INFO, "Skiped read bool");
		}
	      }
	    }
	  }
	}
      }
    }
    // GLOG(INFO, "AfterTouched end");
  }

  void ProbeManager::GetField(uint32_t dex_pc, mirror::Object* obj, ArtField* field, JValue value) {
    if(IsProcDebug()) {
      pthread_mutex_lock(&vector_mutex);
      DBG(DBG_VECTOR_MUTEX) GLOG(INFO, "GetField ");
      if(is_heap_disabled[Thread::Current()->GetIndexInThreadVector()])
	ForceHeapReenable(true);

      mirror::Class* cls = field->GetDeclaringClass();
      std::string temp;
      const char* class_descriptor = cls->GetDescriptor(&temp);
      
      if(class_descriptor[0] != '\0' && field->GetTypeDescriptor()[0] != '\0') {
      	AllocateUntil(valueEv, 1); // caller, value

      	event->set_tid(Thread::Current()->GetTid());
      	event->set_event_address((unsigned long long)dex_pc);

      	if(obj)
      	  FillProtoValue((void*)obj, class_descriptor, valueEv[0]);
      	else /* Static access */
      	  FillProtoValue((unsigned long)obj, class_descriptor, valueEv[0]);
      	FillProtoValue((unsigned long)value.GetJ(), field->GetTypeDescriptor(), valueEv[1]);
	
      	readEv->set_allocated_caller_value(valueEv[0]);
      	readEv->set_field_name(field->GetName());
      	readEv->set_allocated_field_value(valueEv[1]);
      	event->set_allocated_read(readEv);

      	LogEventToSocket(event);

      	readEv->release_caller_value();
      	readEv->release_field_value();
      	event->release_read();
      }

      if(is_heap_disabled[Thread::Current()->GetIndexInThreadVector()])
	ForceHeapDisable(true);

      DBG(DBG_VECTOR_MUTEX) GLOG(INFO, "GetField end");
      pthread_mutex_unlock(&vector_mutex);
    }
  }

  void ProbeManager::SetField(uint32_t dex_pc, mirror::Object* obj, ArtField* field, JValue new_value, JValue old_value) {
    if(IsProcDebug()) {
      pthread_mutex_lock(&vector_mutex);
      DBG(DBG_VECTOR_MUTEX) GLOG(INFO, "SetField");
      if(is_heap_disabled[Thread::Current()->GetIndexInThreadVector()])
	ForceHeapReenable(true);

      mirror::Class* cls = field->GetDeclaringClass();
      std::string temp;
      const char* class_descriptor = cls->GetDescriptor(&temp);

      if(class_descriptor[0] != '\0' && field->GetTypeDescriptor()[0] != '\0') {
	AllocateUntil(valueEv, 2); // caller, first value, new value

	event->set_tid(Thread::Current()->GetTid());
	event->set_event_address((unsigned long long)dex_pc);

	if(obj)
	  FillProtoValue((void*)obj, class_descriptor, valueEv[0]);
	else /* Static access */
	  FillProtoValue((unsigned long)obj, class_descriptor, valueEv[0]);
	FillProtoValue((unsigned long)old_value.GetJ(), field->GetTypeDescriptor(), valueEv[1]);
	FillProtoValue((unsigned long)new_value.GetJ(), field->GetTypeDescriptor(), valueEv[2]);

	writeEv->set_allocated_caller_value(valueEv[0]);
	writeEv->set_field_name(field->GetName());
	writeEv->set_allocated_old_value(valueEv[1]);
	writeEv->set_allocated_new_value(valueEv[2]);
	event->set_allocated_write(writeEv);

	LogEventToSocket(event);

	writeEv->release_caller_value();
	writeEv->release_old_value();
	writeEv->release_new_value();
	event->release_write();
      }

      if(is_heap_disabled[Thread::Current()->GetIndexInThreadVector()])
	ForceHeapDisable(true);

      DBG(DBG_VECTOR_MUTEX) GLOG(INFO, "SetField end");
      pthread_mutex_unlock(&vector_mutex);
    }
  }

  void ProbeManager::GetArray(mirror::Array* array_addr, char const* elementType, size_t component_size) {
    if(IsProcDebug()) {
      unsigned int i=0;
	
      pthread_mutex_lock(&vector_mutex);
      DBG(DBG_VECTOR_MUTEX) GLOG(INFO, "GetArray");
      
      if(is_heap_disabled[Thread::Current()->GetIndexInThreadVector()])
	ForceHeapReenable(true);
      
      event->set_tid(Thread::Current()->GetTid());
      event->set_event_address(GetEventAddrFromJni());

      void* data;
      unsigned long array_length = array_addr->GetLength();
      AllocateUntil(valueEv, array_length);
      ::google::protobuf::RepeatedPtrField< ::protobuf_log::Event_Value >* elemsEv = getArrayEv->mutable_elements();
      for(i=0 ; i<array_length ; i++) {
	data = array_addr->GetRawData(component_size, i);
	FillProtoValue(data, elementType, valueEv[i]);
	elemsEv->AddAllocated(valueEv[i]);
      }
      
      getArrayEv->set_array_addr((unsigned long)array_addr);
      event->set_allocated_getarray(getArrayEv);
      
      LogEventToSocket(event);
      for(i=0 ; i < array_length ; i++)
	  elemsEv->ReleaseLast();
      event->release_getarray();

      if(is_heap_disabled[Thread::Current()->GetIndexInThreadVector()])
	ForceHeapDisable(true);

      DBG(DBG_VECTOR_MUTEX) GLOG(INFO, "GetArray end");
      pthread_mutex_unlock(&vector_mutex);
    }
  }

  // pragma for removing warning when abort is used
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wunreachable-code"
  void ProbeManager::LengthArray(mirror::Array* array_addr) {
    if(IsProcDebug()) {      
      pthread_mutex_lock(&vector_mutex);
      DBG(DBG_VECTOR_MUTEX) GLOG(INFO, "LengthArray");
      
      if(is_heap_disabled[Thread::Current()->GetIndexInThreadVector()])
	ForceHeapReenable(true);
      
      event->set_tid(Thread::Current()->GetTid());
      event->set_event_address(GetEventAddrFromJni());
      lengthArrayEv->set_array_addr((unsigned long)array_addr);
      lengthArrayEv->set_length(array_addr->GetLength());
      event->set_allocated_lengtharray(lengthArrayEv);
      LogEventToSocket(event);
      event->release_lengtharray();

      if(is_heap_disabled[Thread::Current()->GetIndexInThreadVector()])
	ForceHeapDisable(true);

      DBG(DBG_VECTOR_MUTEX) GLOG(INFO, "LengthArray end");
      pthread_mutex_unlock(&vector_mutex);
    }
  }
  #pragma clang diagnostic pop

  // Requieres vector_mutex to be held
  void ProbeManager::ModifyHeapActivationVector(int shift){
    size_t index = Thread::Current()->GetIndexInThreadVector();

    int old_value = heap_activation_vector[index]->top();
    int new_value = old_value + shift;

    if(new_value < 0) {
      // GLOG(INFO, "We have logged more ret than invoke (heap_activation_vector lower than 0)");
    }
    else if(new_value == 0 && old_value == 1) {
      heap_activation_vector[index]->pop();
      heap_activation_vector[index]->push(new_value);
      bool should_enable = true;
      for(std::vector<std::stack<int>*>::iterator it = heap_activation_vector.begin(); it != heap_activation_vector.end(); it++) {
	if((*it)->top() != 0) {
	  should_enable = false;
	  break;
	}
      }
      if(should_enable) {
	BackFromAnalyzedMode();
	// Runtime::Current()->GetHeap()->EnableHeap();
	is_heap_disabled[Thread::Current()->GetIndexInThreadVector()] = false;
      }
    } else if(new_value == 1 && old_value == 0) {
      bool should_disable = true;
      for(std::vector<std::stack<int>*>::iterator it = heap_activation_vector.begin(); it != heap_activation_vector.end(); it++) {
	if((*it)->top() != 0) {
	  should_disable = false;
	  break;
	}
      }
      heap_activation_vector[index]->pop();
      heap_activation_vector[index]->push(new_value);
      if(should_disable) {
	SwitchToAnalyzedMode();
	// Runtime::Current()->GetHeap()->DisableHeap();
	is_heap_disabled[Thread::Current()->GetIndexInThreadVector()] = true;
      }
    } else {
      heap_activation_vector[index]->pop();
      heap_activation_vector[index]->push(new_value);
    }
  }

  // Requieres vector_mutex to be held
  void ProbeManager::ForceHeapDisable(__attribute__((__unused__))bool unlock_signal_manager_heap_mutex) {
    SwitchToAnalyzedMode();
    // Runtime::Current()->GetHeap()->DisableHeap();
    // syscall(SYS_unsafe_stop_other_threads, true);
    // if(unlock_signal_manager_heap_mutex)
    //   pthread_mutex_unlock(&Runtime::Current()->GetSignalManager()->heap_mutex);
  }

  // Requieres vector_mutex to be held
  void ProbeManager::ForceHeapReenable(__attribute__((__unused__))bool lock_signal_manager_heap_mutex) {
    // if(lock_signal_manager_heap_mutex)
    //   pthread_mutex_lock(&Runtime::Current()->GetSignalManager()->heap_mutex);
    // // Avoid dead lock if an other thread gets the logging lock and
    // // the debugged trace tries to log
    // {
    //   MutexLock mu(Thread::Current(), *Locks::logging_lock_);
    //   // syscall(SYS_unsafe_stop_other_threads, false);
    // }
    BackFromAnalyzedMode();
    //Runtime::Current()->GetHeap()->EnableHeap();
  }

  bool ProbeManager::IsWhiteListedMethod(ArtMethod* method) {
    if(IsProcDump()) {
      return !IsDumpedMethod(method);
    }
    else {
      // GLOG(INFO, "Start whitelisted");
      // if(method->HasAnyCompiledCode()) {
	if(method->IsNative()) {
	  const void* native_ptr = method->GetEntryPointFromJni();

	  Dl_info dl_info;
	  if(dladdr(native_ptr, &dl_info) != 0) {
	    for(auto name : whitelisted_libs) {
	      if(strstr(dl_info.dli_fname, name.c_str()) == dl_info.dli_fname + strlen(dl_info.dli_fname) - name.length()) {
		// GLOG(INFO, "Stop 1 whiteListed");
		return true;
	      }
	    }
	    // GLOG(INFO, "[LIB] Not whitelisted: " << dl_info.dli_fname);
	  }
	} else {
	  const DexFile* dex_file = method->GetDexFile();
	  if(dex_file != nullptr) {
	    const std::string& location = dex_file->GetLocation();
	  
	    for(auto name : whitelisted_oats) {
	      if(location.find(name) == 0) {
		// GLOG(INFO, "Stop 2 whiteListed");
		return true;
	      }
	    }
	    // GLOG(INFO, "[OAT] Not whitelisted: " << location << " " << whitelisted_oats.front() << " " << whitelisted_oats.size());
	  }
	// }
      }
      // GLOG(INFO, "Stop 3 whiteListed");
      // GLOG(INFO, "\tDEBUGGED method: " << method->GetName());
      return false;
    }
  }

  void ProbeManager::UntrackEnter(bool lock_mutex) {
    if(IsProcDebug()) {
      size_t index = Thread::Current()->GetIndexInThreadVector();

      if(lock_mutex)
      	pthread_mutex_lock(&vector_mutex);
      DBG(DBG_VECTOR_MUTEX) GLOG(INFO, "UntrackEnter");

      int old_value = heap_activation_vector[index]->top();
      heap_activation_vector[index]->push(0);

      if(old_value > 0) {
      	bool should_enable = true;
      	/*for(std::vector<std::stack<int>*>::iterator it = heap_activation_vector.begin(); it != heap_activation_vector.end(); it++) {
      	  if((*it)->top() != 0) {
      	    should_enable = false;
      	    break;
      	  }
	  }*/

      	if(should_enable) {
	  BackFromAnalyzedMode();
	  // Runtime::Current()->GetHeap()->EnableHeap();
      	  is_heap_disabled[Thread::Current()->GetIndexInThreadVector()] = false;
      	}
      }

      DBG(DBG_VECTOR_MUTEX) GLOG(INFO, "UntrackEnter end");
      if(lock_mutex)
      	pthread_mutex_unlock(&vector_mutex);
    }
  }

  void ProbeManager::UntrackExit(bool lock_mutex) {
    if(IsProcDebug()) {
      size_t index = Thread::Current()->GetIndexInThreadVector();

      if(lock_mutex)
      	pthread_mutex_lock(&vector_mutex);
      DBG(DBG_VECTOR_MUTEX) GLOG(INFO, "UntrackExit");

      if(heap_activation_vector[index]->top() != 0) {
      	GLOG(INFO, "Exit untrack function with heap_activation_vector[index] != 0 => " << heap_activation_vector[index]->top());
      }

      heap_activation_vector[index]->pop();

      if(heap_activation_vector[index]->top() > 0) {
	SwitchToAnalyzedMode();
      	// Runtime::Current()->GetHeap()->DisableHeap();
      	is_heap_disabled[Thread::Current()->GetIndexInThreadVector()] = true;
      }

      DBG(DBG_VECTOR_MUTEX) GLOG(INFO, "UntrackExit end");
      if(lock_mutex)
      	pthread_mutex_unlock(&vector_mutex);
    }
  }

  bool ProbeManager::IsAudited(ArtMethod* method) {
      const char* clsDesc = method->GetDeclaringClassDescriptor();
      if(!strcmp(clsDesc, "Lcom/pg/bench/caes/MainActivity;")
	 && !strcmp(method->GetName(), "aes128")) {
	return true;
      } else if(!strcmp(clsDesc, "Lcom/pg/bench/aes/AES;")
		&& !strcmp(method->GetName(), "solve")) {
	return true;
      }
      // GLOG(INFO, "clsDesc " << clsDesc << " method " << method->GetName());
      return false;
  }

  void ProbeManager::MemMapAllocation(MemMap* mem_map) {
    if(mem_map) {
      if(IsProcDebug()) {
    	MemoryAllocation(mem_map->Begin(), mem_map->Size());
      }
    }
  }

#if defined(__aarch64__)
  void ProbeManager::MemoryAllocation(void* addr, size_t size) {
    if(IsProcDebug()) {
      // unsigned char* ptr = (unsigned char*) addr;
      // unsigned long i;
      // unsigned char tot = 0;
      // for(i=0 ; i<size ; i+=4096) {
      // 	tot += ptr[i];
      // }
      // GLOG(INFO, "tot = " << tot);

      // WARNING: uncomment 
      syscall(SYS_pp_mprotect_b, addr, size, PROT_NONE);
    }
  }
#else
  void ProbeManager::MemoryAllocation(__attribute__((__unused__))void* addr, __attribute__((__unused__))size_t size) {}
#endif


#if defined(__aarch64__)
  void ProbeManager::BackFromAnalyzedMode() {
    if(IsProcDebug()) {
      // Switch to AS A
      // GLOG(INFO, "-> A (heap enabled)");
      syscall(SYS_pp_switch_perms, 1);
    }
  }
  void ProbeManager::SwitchToAnalyzedMode() {
    if(IsProcDebug()) {
      // Switch to AS B
      // GLOG(INFO, "-> B (heap disabled)");
      syscall(SYS_pp_switch_perms, 2);
    }
  }
#else
  void ProbeManager::BackFromAnalyzedMode() {}
  void ProbeManager::SwitchToAnalyzedMode(){}
#endif
  
  }
