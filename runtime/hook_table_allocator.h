#ifndef ART_RUNTIME_HOOK_TABLE_ALLOCATOR_H_
#define ART_RUNTIME_HOOK_TABLE_ALLOCATOR_H_

#include <pthread.h>

#include "utils.h"

namespace art {

  class ArtMethod;
  class OatFile;

  enum HookType : char {
/* #if defined(__aarch64__) */
/*     lockFct = 'L', */
/* #endif    */
    invoke = 'I',
      ret = 'R'
      };

  class HookTableAllocator {

  private:
    void* first_mmap;
    void* last_mmap;
    
    void* last = 0;

    void* mmap_new_page();

    pthread_mutex_t allocator_mutex = PTHREAD_MUTEX_INITIALIZER;

  public:
    struct allocated_entry {
      char instr[4];
      void* quick_code_addr;
      ArtMethod* method;
      const uint8_t* begin_;
      uint32_t code_offset_;
      unsigned int flag;
      HookType hook_type;
      bool thumb;
    };

    void* alloc();
    void fill_entry(struct allocated_entry* entry, void* quick_code, ArtMethod* method, const uint8_t* begin_, uint32_t code_offset_, HookType type, bool thumb);
    void free(void* addr);

    /* safe_check indicate if the address pointed by entry is
       potentially unmapped (if entry is NULL sqfe_check is
       useless) */
    bool checkFlag(struct allocated_entry* entry, bool safe_check=false);
  };
}

#endif  // ART_RUNTIME_HOOK_TABLE_ALLOCATOR_H_
