#ifndef ART_RUNTIME_PROBE_MANAGER_H_
#define ART_RUNTIME_PROBE_MANAGER_H_

#include <pthread.h>
#include <stack>
#include <ucontext.h>
#include <vector>

#include "event_proto2.pb.h"
#include "hook_table_allocator.h"
#include "mirror/string.h"
#include "mirror/throwable.h"
#include "touched_field_cache.h"
#include "thread.h"

#define WHITELISTED_OATS {"/system/framework/"}
#define WHITELISTED_LIBS {"/system/lib/", "/system/lib64/"}

#define DBG_VECTOR_MUTEX false
#define DBG(b) if(b)

namespace art {

  class ArtField;
  class ArtMethod;
  class OatFile;
  class Thread;
  class SignalManager;

  namespace mirror {
    class Class;
    class Object;
  }

  class ProbeManager {
  private:
    bool debugThisProc;
    // 0 => not checked
    // 1 => checked but no debug
    // 2 => checked and debug needed
    unsigned short alreadyCheckDebug;
    uid_t lastUid;
    HookTableAllocator hook_table_allocator;

    char* dumpedMethod;
    uid_t lastUidForDumpCheck;
    bool IsProcDump()
      SHARED_REQUIRES(Locks::mutator_lock_);
    bool IsDumpedMethod(ArtMethod* method)
      SHARED_REQUIRES(Locks::mutator_lock_);

    pthread_mutex_t vector_mutex;
    std::vector<std::stack<int>*> heap_activation_vector;
    std::vector<bool> is_heap_disabled;
    void ModifyHeapActivationVector(int shift)
      SHARED_REQUIRES(Locks::mutator_lock_);
    void ForceHeapDisable(bool unlock_signal_manager_heap_mutex)
      SHARED_REQUIRES(Locks::mutator_lock_);
    void ForceHeapReenable(bool lock_signal_manager_heap_mutex)
      SHARED_REQUIRES(Locks::mutator_lock_);

    std::vector<std::stack<uint32_t>*> dex_pc_vector;

    std::list<std::string> whitelisted_oats WHITELISTED_OATS;
    std::list<std::string> whitelisted_libs WHITELISTED_LIBS;
    bool IsWhiteListedMethod(ArtMethod* method)
      SHARED_REQUIRES(Locks::mutator_lock_);

    static void visitCallback(mirror::Object* obj, void* checked_obj);

    TouchedFieldCache touched_field_cache;
    mirror::Object* touched_object;
    mirror::Class* touched_class;
    ArtField* touched_field;
    void* touched_addr;
    union {
      uint64_t b64;
      uint32_t b32;
      uint16_t b16;
      uint8_t b8;
    } touched_value;

    /* Pre-allocated protobuf structs */
    protobuf_log::Event* event;
    protobuf_log::Event_Read* readEv;
    protobuf_log::Event_Invoke* invokeEv;
    protobuf_log::Event_Write* writeEv;
    protobuf_log::Event_Ret* retEv;
    protobuf_log::Event_NewObj* newEv;
    protobuf_log::Event_Throw* throwEv;
    protobuf_log::Event_Catch* catchEv;
    protobuf_log::Event_MethodLoad* loadEv;
    protobuf_log::Event_Monitor* monitorEv;
    protobuf_log::Event_GetArray* getArrayEv;
    protobuf_log::Event_LengthArray* lengthArrayEv;
    std::vector<protobuf_log::Event_Value*> valueEv;

    template <typename EventType>
      static void AllocateUntil(std::vector<EventType*> &v, int i);

    static void FillProtoValue(unsigned long value_addr, const char* descriptor, protobuf_log::Event_Value* value)
      SHARED_REQUIRES(Locks::mutator_lock_);
    static void FillProtoValue(void* value_addr, const char* descriptor, protobuf_log::Event_Value* value)
      SHARED_REQUIRES(Locks::mutator_lock_);

    /* Logging socket  */
#define LOGGING_PORT_NUMBER 4242
    pthread_mutex_t socket_mutex;
    int socket_fd;

    void LogEventToSocket(protobuf_log::Event* event);

    /*****************************/
    /* Action and event counters */
    /*****************************/

    unsigned int nb_segv=0;
    unsigned int nb_trap=0;

    unsigned int nb_read=0;
    unsigned int nb_write=0;
    unsigned int nb_invoke=0;
    unsigned int nb_ret=0;
    unsigned int nb_new=0;
    unsigned int nb_throw=0;
    unsigned int nb_catch=0;
    unsigned int nb_load=0;

    unsigned int time_protobuf=0;

    bool time_to_count = true;

    bool IsAudited(ArtMethod* method)
      SHARED_REQUIRES(Locks::mutator_lock_);
  public:
    bool IsProcDebug()
      SHARED_REQUIRES(Locks::mutator_lock_);
    ProbeManager();
    ~ProbeManager();
    void Init();

    void OatMethodLinked(ArtMethod* method, const uint8_t* begin_, uint32_t code_offset_, const void* quick_code, bool thumb)
      SHARED_REQUIRES(Locks::mutator_lock_);

    void DeliverException(mirror::Throwable* exception)
      SHARED_REQUIRES(Locks::mutator_lock_);

    void CatchException(uint64_t dex_pc_or_asm_pc, unsigned int nb_skipped_frame)
      SHARED_REQUIRES(Locks::mutator_lock_);

    void ObjectAllocated(mirror::Class* klass, void* allocated_addr)
      SHARED_REQUIRES(Locks::mutator_lock_);

    void MonitorAction(bool enter, mirror::Object* obj)
      SHARED_REQUIRES(Locks::mutator_lock_);

    inline HookTableAllocator* GetHookTableAllocator() {
      return &hook_table_allocator;
    }

    // Return true if the invocation is logged
    bool MethodInvoked(ArtMethod* method, uint32_t* args, uint32_t args_size) // ArtMethod->Invoke
      SHARED_REQUIRES(Locks::mutator_lock_);
    void MethodInvoked(ArtMethod* method, const uint8_t* begin_, uint32_t code_offset_, ucontext_t* u_ctx) // ArtMethod OAT
      SHARED_REQUIRES(Locks::mutator_lock_);
    bool MethodInvoked(ShadowFrame& shadow_frame, const DexFile::CodeItem* code_item) // interpreter
      SHARED_REQUIRES(Locks::mutator_lock_);

    void MethodReturned(ArtMethod* method, JValue* result) // ArtMethod->Invoke
      SHARED_REQUIRES(Locks::mutator_lock_);
    void MethodReturned(ArtMethod* method, ucontext_t* u_ctx) // ArtMethod OAT
      SHARED_REQUIRES(Locks::mutator_lock_);
    void MethodReturned(ShadowFrame& shadow_frame, JValue result) // interpreter
      SHARED_REQUIRES(Locks::mutator_lock_);

    void NewThread(Thread* current);

    void GcTriggered();

    void BeforeTouched(void* instr_addr, void* touched_addr)
      REQUIRES(Locks::mutator_lock_);
    void AfterTouched()
      SHARED_REQUIRES(Locks::mutator_lock_);

    void GetField(uint32_t dex_pc, mirror::Object* obj, ArtField* field, JValue value)
      SHARED_REQUIRES(Locks::mutator_lock_);
    void SetField(uint32_t dex_pc, mirror::Object* obj, ArtField* field, JValue new_value, JValue old_value)
      SHARED_REQUIRES(Locks::mutator_lock_);

    void GetArray(mirror::Array* array_addr, char const* elementType, size_t component_size)
      SHARED_REQUIRES(Locks::mutator_lock_);
    void LengthArray(mirror::Array* array_addr)
      SHARED_REQUIRES(Locks::mutator_lock_);

    void UntrackEnter(bool lock_mutex=false)
      SHARED_REQUIRES(Locks::mutator_lock_);

    void UntrackExit(bool lock_mutex=false)
      SHARED_REQUIRES(Locks::mutator_lock_);

    void MemMapAllocation(MemMap* mem_map)
      NO_THREAD_SAFETY_ANALYSIS; /* TODO: hold mutator_lock_ */
      /* SHARED_REQUIRES(Locks::mutator_lock_); */
    void MemoryAllocation(void* addr, size_t size)
      NO_THREAD_SAFETY_ANALYSIS; /* TODO: hold mutator_lock_ */
      /* SHARED_REQUIRES(Locks::mutator_lock_); */

    void BackFromAnalyzedMode()
      SHARED_REQUIRES(Locks::mutator_lock_);
    void SwitchToAnalyzedMode()
      SHARED_REQUIRES(Locks::mutator_lock_);

    void SetDexPcInVector(uint32_t dexpc, Thread* self=NULL);
    uint32_t GetDexPcInVector(Thread* self=NULL);
    void PopDexPcInVector(Thread* self=NULL);

    friend class SignalManager;
  };
}

#endif  // ART_RUNTIME_PROBE_MANAGER_H_
