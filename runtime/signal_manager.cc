#include "signal_manager.h"

#include <time.h>
#include <signal.h>
#include <ucontext.h>
#include <dlfcn.h>
#include <pthread.h>
#include <stdio.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/resource.h>
#include <sys/stat.h>
#include <sys/syscall.h>
#include <sys/time.h>
#include <sys/types.h>
#include <time.h>

#include "art_field.h"
#include "art_method.h"
#include "disassembler_utils.h"
#include "entrypoints/quick/quick_default_externs.h"
#include "gc/heap.h"
#include "hook_table_allocator.h"
#include "mirror/class.h"
#include "mirror/object.h"
#include "mirror/object-inl.h"
#include "probe_manager.h"
#include "runtime.h"
#include "stride_iterator.h"
#include "utils.h"

#if defined(__arm__)
#define SETPC(new_pc) u_ctx->uc_mcontext.arm_pc = (unsigned long)(new_pc)
#elif defined(__aarch64__)
#define SETPC(new_pc) u_ctx->uc_mcontext.pc = (__u64)(new_pc)
#else
#define SETPC(new_pc)
#endif

namespace art {

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wframe-larger-than="
  unsigned int SignalManager::DumpProccessToFile(bool threads_already_paused) {
    static int nb_dump = 0;
    unsigned int ret_no = 0;

    GLOG(INFO, "Dump requested");

    // Freeze other threads
    if(!threads_already_paused)
      syscall(SYS_unsafe_stop_other_threads, false);

    nb_dump++;

    mode_t previous_umask = umask(S_IXUSR | S_IXGRP | S_IXOTH);

    char path[60];
    snprintf(path, 60, "%s%i", DUMP_FILE_PATH, nb_dump);

    FILE* dump_file = fopen(path, "w");
    if(dump_file != NULL) {
      FILE* file = fopen("/proc/self/maps", "r");
      if(file != NULL) {
	/* Copy from kernel: tools/vm/page-types.c, "parse_pid" */
	char buf[4000];
	while (fgets(buf, sizeof(buf), file) != NULL) {
	  unsigned long vm_start;
	  unsigned long vm_end;
	  unsigned long long pgoff;
	  int major, minor;
	  char r, w, x, s;
	  unsigned long ino;
	  int n;

	  n = sscanf(buf, "%lx-%lx %c%c%c%c %llx %x:%x %lu",
		     &vm_start,
		     &vm_end,
		     &r, &w, &x, &s,
		     &pgoff,
		     &major, &minor,
		     &ino);
	  if (n < 10) {
	    GLOG(INFO, "ERROR: unexpected line: " << buf);
	    continue;
	  }

	  bool go_dump_this_line = false;

	  struct stat sb;
	  *strrchr(buf, '\n') = '\0';
	  char* dev_path = strrchr(buf, ' ')+1;
	  if(dev_path[0] == '/') {
	    if(stat(dev_path, &sb) == 0) {
	      if(S_ISREG(sb.st_mode)) {
		go_dump_this_line = true;
	      }
	    } else {
	      GLOG(INFO, "stat(" << dev_path << "): " << strerror(errno));
	    }
	  } else
	    go_dump_this_line = true;

	  if(go_dump_this_line) {
	    int prot = PROT_NONE;
	    prot |= r=='r' ? PROT_READ : PROT_NONE;
	    prot |= w=='w' ? PROT_WRITE : PROT_NONE;
	    prot |= x=='x' ? PROT_EXEC : PROT_NONE;

	    if(r != 'r') {
	      mprotect((void*)vm_start, vm_end-vm_start, prot | PROT_READ);
	    }

	    // TODO: check the dump file is not mapped

	    GLOG(INFO, "Dumping " << (void*) vm_start << "-" << (void*)vm_end << " " << r << w << x);
	    fprintf(dump_file, "%lx-%lx %c%c%c\n", vm_start, vm_end, r, w, x);
	    unsigned int ret_write = fwrite((void*)vm_start, 1, vm_end-vm_start, dump_file);
	    if(ret_write != vm_end-vm_start) {
	      GLOG(INFO, "fwrite: " << strerror(errno));
	      GLOG(INFO, buf);
	    }

	    if(r != 'r') {
	      mprotect((void*)vm_start, vm_end-vm_start, prot);
	    }
	  }
	}
	fclose(file);
      }
      else {
	GLOG(INFO, "Can not open /proc/self/maps: " << strerror(errno));
      }
      fclose(dump_file);
    } else {
      GLOG(INFO, "Can not open " << path << ": " << strerror(errno));
    }

    umask(previous_umask);

    ret_no = nb_dump;

    // Unfreeze other threads
    if(!threads_already_paused)
      syscall(SYS_unsafe_stop_other_threads, true);

    return ret_no;
  }
#pragma clang diagnostic pop

  void SignalManager::trapHandler(int signum, siginfo_t * info, void * ctx)
    NO_THREAD_SAFETY_ANALYSIS  {
    SignalManager* sm  = Runtime::Current()->GetSignalManager();
    ProbeManager* pm = Runtime::Current()->GetProbeManager();

    if(pm->time_to_count){
      pm->nb_trap++;
    }

    /* Doc in: */
    /* prebuilts/ndk/current/platforms/android-$version$/$arch$/usr/include/sys/ucontext.h */

    /* Get program counter (instruction pointer) from ucontext */
    ucontext_t* u_ctx = (ucontext_t *) ctx;
#if defined(__arm__)
    unsigned long ip_addr = u_ctx->uc_mcontext.arm_pc;
#elif defined(__aarch64__)
    __u64 ip_addr = u_ctx->uc_mcontext.pc;
#elif defined(__i386__)
    unsigned long ip_addr = u_ctx->uc_mcontext.gregs[REG_EIP];
#elif defined(__x86_64__)
    __u64 ip_addr = u_ctx->uc_mcontext.gregs[REG_RIP];
#elif defined(__mips__)
    __u64 ip_addr = u_ctx->uc_mcontext.sc_pc;
#else
#error "arch not handled"
#endif

#if defined(__arm__)
    if(ip_addr == (unsigned long)((char*)sm->sandbox + 2) || ip_addr == (unsigned long)((char*)sm->sandbox + 8) || ip_addr == (unsigned long)((char*)sm->sandbox + 14)) {
#elif defined(__aarch64__)
    if(ip_addr == (__u64)((char*)sm->sandbox + 4)) {
#else
    if(false) {
#endif
      // GLOG(INFO, "returned from sandbox");
#if defined(__arm__) || defined(__aarch64__)
      
      Runtime::Current()->GetProbeManager()->AfterTouched();

      // Disable Heap
      Runtime::Current()->GetProbeManager()->SwitchToAnalyzedMode();
      // Runtime::Current()->GetHeap()->DisableHeap();
      Runtime::Current()->GetProbeManager()->is_heap_disabled[Thread::Current()->GetIndexInThreadVector()] = true;

      // // Unfreeze other threads
      // syscall(SYS_unsafe_stop_other_threads, true);

      // Modify PC after the faulty instruction
      SETPC(sm->sandbox_return_addr);

#if defined(__aarch64__)
      if(sm->stx_status_register != -1) {
	// Set Wstatus to 0 (High level bits not touched)
	u_ctx->uc_mcontext.regs[sm->stx_status_register] = u_ctx->uc_mcontext.regs[sm->stx_status_register] & ((__u64)0xffffffff << 32);

	// Reset Wstatus number to modify this reg only one time
	sm->stx_status_register = -1;
      }
#endif
      
      // Release signal manager mutex
      // pthread_mutex_unlock(&sm->heap_mutex);

      // Release probe manager mutex
      DBG(DBG_VECTOR_MUTEX) GLOG(INFO, "Sandbox end");
      pthread_mutex_unlock(&Runtime::Current()->GetProbeManager()->vector_mutex);
#endif
    }
    else {
      HookTableAllocator* hta = pm->GetHookTableAllocator();
      if(hta->checkFlag((struct HookTableAllocator::allocated_entry*)ip_addr)) {
	struct HookTableAllocator::allocated_entry* entry = (struct HookTableAllocator::allocated_entry*) ip_addr;

	if(entry->hook_type == HookType::invoke) {
	  pm->MethodInvoked(entry->method, entry->begin_, entry->code_offset_, u_ctx);
	  SETPC(entry->quick_code_addr);
	  
	  struct HookTableAllocator::allocated_entry* new_hook = (struct HookTableAllocator::allocated_entry*) hta->alloc();
#if defined(__arm__)
	  void* lr = (void*)u_ctx->uc_mcontext.arm_lr;
	  if(entry->thumb)
	    u_ctx->uc_mcontext.arm_lr = (unsigned long) new_hook | 1;
	  else
	    u_ctx->uc_mcontext.arm_lr = (unsigned long) new_hook;
#elif defined(__aarch64__)
	  void* lr = (void*)u_ctx->uc_mcontext.regs[30]; // x30 = lr
	  u_ctx->uc_mcontext.regs[30] = (__u64) new_hook;
#else
	  /* TODO: implem for x86 and mips */
	  void* lr = 0;
#endif
	  hta->fill_entry(new_hook, lr, entry->method, entry->begin_, entry->code_offset_, HookType::ret, entry->thumb);
	}
	  
	else if(entry->hook_type == HookType::ret) {
	  SETPC(entry->quick_code_addr);
	  pm->MethodReturned(entry->method, u_ctx);
	  hta->free(entry);
	}

	else {
	  GLOG(INFO, "ERROR: hooked trap with incorrect hook type: " << entry->hook_type);
	}
	// GLOG(INFO, "End hook table");
      }
      else {
	GLOG(INFO, "TRAP not handled");

	if(sm->oldactTRAP.sa_flags | SA_SIGINFO) {
	  if(sm->oldactTRAP.sa_sigaction != NULL)
	    sm->oldactTRAP.sa_sigaction(signum, info, ctx);
	}
	else {
	  if(sm->oldactTRAP.sa_handler != NULL)
	    sm->oldactTRAP.sa_handler(signum);
	}
      }
    }
  }

  void SignalManager::segvHandler(int signum, siginfo_t * info, void * ctx)
    NO_THREAD_SAFETY_ANALYSIS {

    SignalManager* sm = Runtime::Current()->GetSignalManager();
    
    if (info->si_code == SEGV_ACCERR) {

// #if defined(__aarch64__)      
//       GLOG(INFO, "Treated faulty pc @ " << (void*)(((ucontext_t *)ctx)->uc_mcontext.pc));
// #endif
      
      // // if we are not a debugging thread we pause
      // size_t vector_id = Thread::Current()->GetIndexInThreadVector();
      // if(Runtime::Current()->GetProbeManager()->heap_activation_vector[vector_id]->top() <= 0) {
      // 	GLOG(INFO, "I am not targetted :(");
      // 	struct timespec ts;
      // 	ts.tv_sec = 0;
      // 	ts.tv_nsec = 10 * 1000000; // millisec
      // 	nanosleep(&ts, NULL);
      // }

      // If the thread was in 

      // Hold probe manager mutex
      int error = pthread_mutex_lock(&Runtime::Current()->GetProbeManager()->vector_mutex);
      if(error != 0) {
	if(error == EDEADLK) {
	  GLOG(INFO, "This thread already lock vector_mutex !!!");
	}

	GLOG(INFO, "Is heap desactivated " << Runtime::Current()->GetProbeManager()->is_heap_disabled[Thread::Current()->GetIndexInThreadVector()]);
	// Abort for stack-tracing
	// Runtime::Current()->GetProbeManager()->BackFromAnalyzedMode();
      	abort();	
      }
      DBG(DBG_VECTOR_MUTEX) GLOG(INFO, "segvHandler start");
      // GLOG(INFO, "Sandbox");
      
      // Hold signal manager mutex
      // pthread_mutex_lock(&sm->heap_mutex);

      // // Avoid dead lock if an other thread gets the logging lock and
      // // the debugged trace tries to log
      // {
      // 	MutexLock mu(Thread::Current(), *Locks::logging_lock_);
      // 	// // Freeze other threads
      // 	// syscall(SYS_unsafe_stop_other_threads, false);
      // }

      // Enable heap
      Runtime::Current()->GetProbeManager()->BackFromAnalyzedMode();
      // Runtime::Current()->GetHeap()->EnableHeap();
      Runtime::Current()->GetProbeManager()->is_heap_disabled[Thread::Current()->GetIndexInThreadVector()] = false;

      if(Runtime::Current()->GetProbeManager()->time_to_count){
	Runtime::Current()->GetProbeManager()->nb_segv++;
      }
      
      // Copy current instruction in the sandbox and modify PC to the
      // sandbox addr
#if defined(__arm__)
      ucontext_t* u_ctx = (ucontext_t *) ctx;
      bool thumb = (u_ctx->uc_mcontext.arm_cpsr & (1<<5)) == 1<<5;
      unsigned long ip_addr = u_ctx->uc_mcontext.arm_pc;
      unsigned int patched_instr = *(unsigned int*)ip_addr;
      void* touched_addr = (void*) info->si_addr;

      // GLOG(INFO, "thumb " << thumb);
      // GLOG(INFO, "patched_instr " << patched_instr);

      Runtime::Current()->GetProbeManager()->BeforeTouched((void*) ip_addr, touched_addr);

      if(thumb) {
	bool is32bytes = false;
	if((patched_instr  & (0x1f<<3)) == (0x1d<<3) )
	  is32bytes = true;
	else if((patched_instr  & (0x1f<<3)) == (0x1e<<3) )
	  is32bytes = true;
	else if((patched_instr & (0x1f<<3)) == (0x1f<<3) )
	  is32bytes = true;

	if(is32bytes) {
	  *(unsigned int*) ((char*)sm->sandbox + 4) = patched_instr;
	  FlushInstructionCache((char*)sm->sandbox + 4, (char*)sm->sandbox + 8);

	  SETPC((char*)sm->sandbox + 4);
	  sm->sandbox_return_addr =  (void*)(ip_addr + 4);
	}
	else {
	  ((char*) sm->sandbox)[0] = patched_instr >> 24;
	  ((char*) sm->sandbox)[1] = (patched_instr >> 16) & 0xff;
	  FlushInstructionCache((char*)sm->sandbox, (char*)sm->sandbox + 2);

	  SETPC(sm->sandbox);
	  sm->sandbox_return_addr =  (void*)(ip_addr + 2);
	}
      }
      else {
	*(unsigned int*) ((char*)sm->sandbox + 10) = patched_instr;
	FlushInstructionCache((char*)sm->sandbox + 10, (char*)sm->sandbox + 14);
	SETPC((char*)sm->sandbox + 10);
	sm->sandbox_return_addr =  (void*)(ip_addr + 4);
      }
#elif defined(__aarch64__)
      ucontext_t* u_ctx = (ucontext_t *) ctx;
      __u64 ip_addr = u_ctx->uc_mcontext.pc;
      unsigned int patched_instr = *(unsigned int*) ip_addr;
      void* touched_addr = (void*) info->si_addr;

      Runtime::Current()->GetProbeManager()->BeforeTouched((void*) ip_addr, touched_addr);

      // We have to check if the faulty instruction correspond to ldx
      // instructions to remember the last exclusively loaded address
      // per thread
      if( is_arm64_ldx_instruction(patched_instr) ) {
	// Get mutex to be safe
	pthread_mutex_lock(&sm->excl_addr_vector_mutex);
	
	// Update the last exclusively loaded address
	size_t thread_index = Thread::Current()->GetIndexInThreadVector();
	sm->last_exclusive_addr_vector[thread_index] = touched_addr;

	// Invalidate other thread that loaded same address
	// (last setted address set to zero)
	for(std::vector<void*>::size_type i = 0; i != sm->last_exclusive_addr_vector.size(); i++)
	  if(i != thread_index && sm->last_exclusive_addr_vector[i] == touched_addr)
	    sm->last_exclusive_addr_vector[i] = 0;

	// Relese mutex
	pthread_mutex_unlock(&sm->excl_addr_vector_mutex);
      }
      // We have to check if the faulty instruction correspond to stx
      // instructions to effectively run the store if the last
      // exclusively loaded address correspond the the current faulty
      // one
      else if( is_arm64_stx_instruction(patched_instr) ) {
	// Get mutex to be safe
	pthread_mutex_lock(&sm->excl_addr_vector_mutex);

	// If the last loaded address is the loaded address then the
	// patched instruction (the one which is ran) should be "st"
	// instead of "stx"
	size_t thread_index = Thread::Current()->GetIndexInThreadVector();
	if( sm->last_exclusive_addr_vector[thread_index] == touched_addr ) {
	  // Change the patch_instr
	  patched_instr = get_corresponding_arm64_not_exclusive_store(patched_instr, &sm->stx_status_register);
	}

	// Relese mutex
	pthread_mutex_unlock(&sm->excl_addr_vector_mutex);
      }
      // Else we load a new address and we have to reset the last exclusive load
      else
	sm->last_exclusive_addr_vector[Thread::Current()->GetIndexInThreadVector()] = 0;

      *(unsigned int*) sm->sandbox = patched_instr;
      FlushInstructionCache((char*)sm->sandbox, (char*)sm->sandbox + 4);

      SETPC(sm->sandbox);
      sm->sandbox_return_addr = (void*)(ip_addr + 4);
#endif
      // GLOG(INFO, "Go to sandbox");
    }
    else {
#if defined(__arm__)
      ucontext_t* uu_ctx = (ucontext_t *) ctx;
      GLOG(INFO, "instr " << (void*)*(unsigned int*) uu_ctx->uc_mcontext.arm_pc);
    
      GLOG(INFO, "r0 " << (void*) uu_ctx->uc_mcontext.arm_r0);
      GLOG(INFO, "r1 " << (void*) uu_ctx->uc_mcontext.arm_r1);
      GLOG(INFO, "r2 " << (void*) uu_ctx->uc_mcontext.arm_r2);
      GLOG(INFO, "r3 " << (void*) uu_ctx->uc_mcontext.arm_r3);
      GLOG(INFO, "r4 " << (void*) uu_ctx->uc_mcontext.arm_r4);
      GLOG(INFO, "r5 " << (void*) uu_ctx->uc_mcontext.arm_r5);
      GLOG(INFO, "r6 " << (void*) uu_ctx->uc_mcontext.arm_r6);
      GLOG(INFO, "r7 " << (void*) uu_ctx->uc_mcontext.arm_r7);
      GLOG(INFO, "r8 " << (void*) uu_ctx->uc_mcontext.arm_r8);
      GLOG(INFO, "r9 " << (void*) uu_ctx->uc_mcontext.arm_r9);
      GLOG(INFO, "r10 " << (void*) uu_ctx->uc_mcontext.arm_r10);
      GLOG(INFO, "fp " << (void*) uu_ctx->uc_mcontext.arm_fp);
      GLOG(INFO, "ip " << (void*) uu_ctx->uc_mcontext.arm_ip);
      GLOG(INFO, "sp " << (void*) uu_ctx->uc_mcontext.arm_sp);
      GLOG(INFO, "lr " << (void*) uu_ctx->uc_mcontext.arm_lr);
      GLOG(INFO, "pc " << (void*) uu_ctx->uc_mcontext.arm_pc);
      GLOG(INFO, "cpsr " << (void*) uu_ctx->uc_mcontext.arm_cpsr);
      GLOG(INFO, "fault_address " << (void*) uu_ctx->uc_mcontext.fault_address);
#endif

      GLOG(INFO, "SEGV @ " << (void*) info->si_addr << " end go to default " << signum << " " << info->si_code << " " << ctx << " " << sm);

#if defined(__aarch64__)
      for(unsigned int i = 0 ; i < 8 ; i++) {
	if(i != 7) {
	  GLOG(INFO, "r" << i*4 << " = " << (void*)(((ucontext_t *)ctx)->uc_mcontext.regs[i*4])
	       << "\tr" << i*4+1 << " = " << (void*)(((ucontext_t *)ctx)->uc_mcontext.regs[i*4+1])
	       << "\tr" << i*4+2 << " = " << (void*)(((ucontext_t *)ctx)->uc_mcontext.regs[i*4+2])
	       << "\tr" << i*4+3 << " = " << (void*)(((ucontext_t *)ctx)->uc_mcontext.regs[i*4+3]));
	} else {
	  GLOG(INFO, "r" << i*4 << " = " << (void*)(((ucontext_t *)ctx)->uc_mcontext.regs[i*4])
	       << "\tr" << i*4+1 << " = " << (void*)(((ucontext_t *)ctx)->uc_mcontext.regs[i*4+1])
	       << "\tr" << i*4+2 << " = " << (void*)(((ucontext_t *)ctx)->uc_mcontext.regs[i*4+2]));
	}
      }
      GLOG(INFO, "PC is = " << (void*)(((ucontext_t *)ctx)->uc_mcontext.pc));
#endif

      GLOG(INFO, "Is heap desactivated " << Runtime::Current()->GetProbeManager()->is_heap_disabled[Thread::Current()->GetIndexInThreadVector()]);
      Runtime::Current()->GetProbeManager()->BackFromAnalyzedMode();
      
      // Abort for stack-tracing
      abort();
      
      //   if(sm->oldactSEGV.sa_flags | SA_SIGINFO) {
      //   	if(sm->oldactSEGV.sa_sigaction != NULL)
      //   	  sm->oldactSEGV.sa_sigaction(signum, info, ctx);
      //   }
      //   else {
      //   	if(sm->oldactSEGV.sa_handler != NULL)
      //   	  sm->oldactSEGV.sa_handler(signum);
      //   }
    }
  }

#if defined(__aarch64__)
  struct fpsimd_context* SignalManager::GetFpsimdFromContext(const ucontext_t* ctx) {
    struct fpsimd_context* fpsimd_ctx = NULL;
    void* start = (void*) &ctx->uc_mcontext.__reserved;
    bool keep_going=true;
    while(keep_going) {
      __u32 ctx_size = *(__u32*)((char*)start + sizeof(__u32));
      switch(*(__u32*)start) {
      case FPSIMD_MAGIC:
	fpsimd_ctx = (struct fpsimd_context*)((char*)start + ctx_size);
	keep_going = false;
	break;
      case 0:
	keep_going = false;
	break;
      default:
	start = (void*)((char*)start + ctx_size);
      }
    }
    return fpsimd_ctx;
  }
#endif

  void SignalManager::Init() NO_THREAD_SAFETY_ANALYSIS {

#if defined(__aarch64__)
    stx_status_register = -1;
#endif

    /* From sigchain.cc */
    /* Retrieve the original sigaction function, not the hooked one */
    int (*linked_sigaction)(int, const struct sigaction*, struct sigaction*);
    void * linked_sigaction_sym = dlsym(RTLD_NEXT, "sigaction");
    if (linked_sigaction_sym == nullptr) {
      linked_sigaction_sym = dlsym(RTLD_DEFAULT, "sigaction");
      if (linked_sigaction_sym == nullptr ||
	  linked_sigaction_sym == reinterpret_cast<void*>(sigaction)) {
	linked_sigaction_sym = nullptr;
      }
    }
    linked_sigaction = (int(*)(int, const struct sigaction*, struct sigaction*)) linked_sigaction_sym;

    /* Setup handler for SIGEGV */
    struct sigaction act;
    act.sa_sigaction = SignalManager::segvHandler;
    sigemptyset(&act.sa_mask);
    act.sa_flags = SA_SIGINFO;
    act.sa_flags |= SA_RESTART;
    act.sa_flags |= SA_ONSTACK;
    if(linked_sigaction(SIGSEGV, &act, &oldactSEGV) == -1)
      GLOG(INFO, "Error in SIGSEGV sigaction");
    
    /* Setup handler for SIGTRAP */
    act.sa_sigaction = SignalManager::trapHandler;
    sigemptyset(&act.sa_mask);
    act.sa_flags = SA_SIGINFO;
    act.sa_flags |= SA_RESTART;
    act.sa_flags |= SA_ONSTACK;
    if(linked_sigaction(SIGTRAP, &act, &oldactTRAP) == -1)
      GLOG(INFO, "Error in SIGTRAP sigaction");

    long res = syscall(SYS_tracing, SignalManager::segvHandler, SignalManager::trapHandler);
    if(res == -1) {
      GLOG(INFO, "Error during SYS_tracing: " << strerror(errno));
    }
    
    // heap_mutex = PTHREAD_MUTEX_INITIALIZER;
#if defined(__aarch64__)
    excl_addr_vector_mutex = PTHREAD_MUTEX_INITIALIZER;
#endif
    
#if defined(__arm__)
    // [0-1]: instr thumb, [2-3]: bp thumb
    // [4-7]: instr thumb, [8-9]: bp thumb
    // [10-13]: instr arm, [14-17]: bp arm
    sandbox = mmap(NULL, 18, PROT_READ | PROT_WRITE | PROT_EXEC, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);

    ((char*)sandbox)[2] = '\xbe';
    ((char*)sandbox)[3] = '\xbe';
    
    ((char*)sandbox)[8] = '\xbe';
    ((char*)sandbox)[9] = '\xbe';
    
    ((char*)sandbox)[17] = '\xe1';
    ((char*)sandbox)[16] = '\x20';
    ((char*)sandbox)[15] = '\x00';
    ((char*)sandbox)[14] = '\x70';
#elif defined(__aarch64__)
    // [0-3]: instr, [4-7]: bp
    sandbox = mmap(NULL, 8, PROT_READ | PROT_WRITE | PROT_EXEC, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
    ((char*)sandbox)[7] = '\xd4';
    ((char*)sandbox)[6] = '\x20';    
    ((char*)sandbox)[5] = '\x00';
    ((char*)sandbox)[4] = '\x00';
#endif
  }
}


void do_wrap_enter(void* lr) {
// #if defined(__aarch64__)
//   GLOG(art::INFO, "ENTER aarch64 " << lr);
// #elif defined(__arm__)
//   GLOG(art::INFO, "ENTER arm " << lr);
// #else
//   GLOG(art::INFO, "ENTER ?!? " << lr);
// #endif
  art::Runtime::Current()->GetProbeManager()->UntrackEnter(true);
  size_t i = art::Thread::Current()->GetIndexInThreadVector();
  art::Runtime::Current()->GetSignalManager()->wrapper_lr[i]->push(lr);
}

void* do_wrap_exit() {
  size_t i = art::Thread::Current()->GetIndexInThreadVector();
  void* old_lr = art::Runtime::Current()->GetSignalManager()->wrapper_lr[i]->top();
  art::Runtime::Current()->GetSignalManager()->wrapper_lr[i]->pop();
// #if defined(__aarch64__)
//   GLOG(art::INFO, "EXIT aarch64 " << old_lr);
// #elif defined(__arm__)
//   GLOG(art::INFO, "EXIT arm " << old_lr);
// #else
//   GLOG(art::INFO, "EXIT ?!? " << old_lr);
// #endif
  art::Runtime::Current()->GetProbeManager()->UntrackExit(true);
  return old_lr;
}
  
#undef SETPC
