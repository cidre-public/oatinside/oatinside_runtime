# usage:
# python2 generate_wrapper.py
# in art/runtime/arch/ directory

function_mapping_arm = {}
function_mapping_arm64 = {}
function_mapping_x86 = {}
function_mapping_x86_64 = {}
function_mapping_mips = {}
function_mapping_mips64 = {}

args_mapping = {}

def add_in_list_no_suffix(dic, k, v):
    l = dic.get(k, [])
    if v not in l:
        l.append(v)
    dic[k] = l

def add_in_list(arch, k, v):
    if arch == "quick":
        dics = [function_mapping_arm, function_mapping_arm64, function_mapping_x86,
               function_mapping_x86_64, function_mapping_mips, function_mapping_mips64]
    if arch == "arm":
        dics = [function_mapping_arm]
    if arch == "arm64":
        dics = [function_mapping_arm64]
    if arch == "x86":
        dics = [function_mapping_x86]
    if arch == "x86_64":
        dics = [function_mapping_x86_64]
    if arch == "mips":
        dics = [function_mapping_mips]
    if arch == "mips64":
        dics = [function_mapping_mips64]
    if "##suffix" in v:
        for s in ['_dlmalloc', '_rosalloc', '_bump_pointer', '_tlab', '_region', '_region_tlab']:
            for dic in dics:
                add_in_list_no_suffix(dic, k ,v.replace('##suffix', s).replace(s+'##',s))
    else:
        for dic in dics:
            add_in_list_no_suffix(dic, k ,v)

# The following instructions are generated using:
# grep -or "qpoints->.*;" | grep -v nullptr | sed "s/->p/->/" | sed "s/::/_#_#_/" | sed "s/\//:/g" | cut -d ':' -f 2,4 | sed "s/:/>/" | cut -d '>' -f 1,3 | sed "s/>/', '/" | sed "s/wrap_//" | sed "s/ = /', '/" | sed "s/^/add_in_list('/" | sed "s/;/')/" | sed "s/_#_#_/::/"
# in the art/runtime/ directory

add_in_list('quick', 'InitializeStaticStorage', 'art_quick_initialize_static_storage')
add_in_list('quick', 'InitializeTypeAndVerifyAccess', 'art_quick_initialize_type_and_verify_access')
add_in_list('quick', 'InitializeType', 'art_quick_initialize_type')
add_in_list('quick', 'ResolveString', 'art_quick_resolve_string')
add_in_list('quick', 'Set8Instance', 'art_quick_set8_instance')
add_in_list('quick', 'Set8Static', 'art_quick_set8_static')
add_in_list('quick', 'Set16Instance', 'art_quick_set16_instance')
add_in_list('quick', 'Set16Static', 'art_quick_set16_static')
add_in_list('quick', 'Set32Instance', 'art_quick_set32_instance')
add_in_list('quick', 'Set32Static', 'art_quick_set32_static')
add_in_list('quick', 'Set64Instance', 'art_quick_set64_instance')
add_in_list('quick', 'Set64Static', 'art_quick_set64_static')
add_in_list('quick', 'SetObjInstance', 'art_quick_set_obj_instance')
add_in_list('quick', 'SetObjStatic', 'art_quick_set_obj_static')
add_in_list('quick', 'GetByteInstance', 'art_quick_get_byte_instance')
add_in_list('quick', 'GetBooleanInstance', 'art_quick_get_boolean_instance')
add_in_list('quick', 'GetShortInstance', 'art_quick_get_short_instance')
add_in_list('quick', 'GetCharInstance', 'art_quick_get_char_instance')
add_in_list('quick', 'Get32Instance', 'art_quick_get32_instance')
add_in_list('quick', 'Get64Instance', 'art_quick_get64_instance')
add_in_list('quick', 'GetObjInstance', 'art_quick_get_obj_instance')
add_in_list('quick', 'GetByteStatic', 'art_quick_get_byte_static')
add_in_list('quick', 'GetBooleanStatic', 'art_quick_get_boolean_static')
add_in_list('quick', 'GetShortStatic', 'art_quick_get_short_static')
add_in_list('quick', 'GetCharStatic', 'art_quick_get_char_static')
add_in_list('quick', 'Get32Static', 'art_quick_get32_static')
add_in_list('quick', 'Get64Static', 'art_quick_get64_static')
add_in_list('quick', 'GetObjStatic', 'art_quick_get_obj_static')
add_in_list('quick', 'AputObjectWithNullAndBoundCheck', 'art_quick_aput_obj_with_null_and_bound_check')
add_in_list('quick', 'AputObjectWithBoundCheck', 'art_quick_aput_obj_with_bound_check')
add_in_list('quick', 'AputObject', 'art_quick_aput_obj')
add_in_list('quick', 'HandleFillArrayData', 'art_quick_handle_fill_data')
add_in_list('quick', 'JniMethodStart', 'JniMethodStart')
add_in_list('quick', 'JniMethodStartSynchronized', 'JniMethodStartSynchronized')
add_in_list('quick', 'JniMethodEnd', 'JniMethodEnd')
add_in_list('quick', 'JniMethodEndSynchronized', 'JniMethodEndSynchronized')
add_in_list('quick', 'JniMethodEndWithReference', 'JniMethodEndWithReference')
add_in_list('quick', 'JniMethodEndWithReferenceSynchronized', 'JniMethodEndWithReferenceSynchronized')
add_in_list('quick', 'QuickGenericJniTrampoline', 'art_quick_generic_jni_trampoline')
add_in_list('quick', 'LockObject', 'art_quick_lock_object_no_inline')
add_in_list('quick', 'UnlockObject', 'art_quick_unlock_object_no_inline')
add_in_list('quick', 'LockObject', 'art_quick_lock_object')
add_in_list('quick', 'UnlockObject', 'art_quick_unlock_object')
add_in_list('quick', 'QuickImtConflictTrampoline', 'art_quick_imt_conflict_trampoline')
add_in_list('quick', 'QuickResolutionTrampoline', 'art_quick_resolution_trampoline')
add_in_list('quick', 'QuickToInterpreterBridge', 'art_quick_to_interpreter_bridge')
add_in_list('quick', 'TestSuspend', 'art_quick_test_suspend')
add_in_list('quick', 'DeliverException', 'art_quick_deliver_exception')
add_in_list('quick', 'ThrowArrayBounds', 'art_quick_throw_array_bounds')
add_in_list('quick', 'ThrowDivZero', 'art_quick_throw_div_zero')
add_in_list('quick', 'ThrowNoSuchMethod', 'art_quick_throw_no_such_method')
add_in_list('quick', 'ThrowNullPointer', 'art_quick_throw_null_pointer_exception')
add_in_list('quick', 'ThrowStackOverflow', 'art_quick_throw_stack_overflow')
add_in_list('quick', 'Deoptimize', 'art_quick_deoptimize_from_compiled_code')
add_in_list('quick', 'AllocArray', 'art_quick_alloc_array##suffix##_instrumented')
add_in_list('quick', 'AllocArrayResolved', 'art_quick_alloc_array_resolved##suffix##_instrumented')
add_in_list('quick', 'AllocArrayWithAccessCheck', 'art_quick_alloc_array_with_access_check##suffix##_instrumented')
add_in_list('quick', 'AllocObject', 'art_quick_alloc_object##suffix##_instrumented')
add_in_list('quick', 'AllocObjectResolved', 'art_quick_alloc_object_resolved##suffix##_instrumented')
add_in_list('quick', 'AllocObjectInitialized', 'art_quick_alloc_object_initialized##suffix##_instrumented')
add_in_list('quick', 'AllocObjectWithAccessCheck', 'art_quick_alloc_object_with_access_check##suffix##_instrumented')
add_in_list('quick', 'CheckAndAllocArray', 'art_quick_check_and_alloc_array##suffix##_instrumented')
add_in_list('quick', 'CheckAndAllocArrayWithAccessCheck', 'art_quick_check_and_alloc_array_with_access_check##suffix##_instrumented')
add_in_list('quick', 'AllocStringFromBytes', 'art_quick_alloc_string_from_bytes##suffix##_instrumented')
add_in_list('quick', 'AllocStringFromChars', 'art_quick_alloc_string_from_chars##suffix##_instrumented')
add_in_list('quick', 'AllocStringFromString', 'art_quick_alloc_string_from_string##suffix##_instrumented')
add_in_list('quick', 'AllocArray', 'art_quick_alloc_array##suffix')
add_in_list('quick', 'AllocArrayResolved', 'art_quick_alloc_array_resolved##suffix')
add_in_list('quick', 'AllocArrayWithAccessCheck', 'art_quick_alloc_array_with_access_check##suffix')
add_in_list('quick', 'AllocObject', 'art_quick_alloc_object##suffix')
add_in_list('quick', 'AllocObjectResolved', 'art_quick_alloc_object_resolved##suffix')
add_in_list('quick', 'AllocObjectInitialized', 'art_quick_alloc_object_initialized##suffix')
add_in_list('quick', 'AllocObjectWithAccessCheck', 'art_quick_alloc_object_with_access_check##suffix')
add_in_list('quick', 'CheckAndAllocArray', 'art_quick_check_and_alloc_array##suffix')
add_in_list('quick', 'CheckAndAllocArrayWithAccessCheck', 'art_quick_check_and_alloc_array_with_access_check##suffix')
add_in_list('quick', 'AllocStringFromBytes', 'art_quick_alloc_string_from_bytes##suffix')
add_in_list('quick', 'AllocStringFromChars', 'art_quick_alloc_string_from_chars##suffix')
add_in_list('quick', 'AllocStringFromString', 'art_quick_alloc_string_from_string##suffix')
add_in_list('arm', 'InstanceofNonTrivial', 'artIsAssignableFromCode')
add_in_list('arm', 'CheckCast', 'art_quick_check_cast')
add_in_list('arm', 'Idivmod', '__aeabi_idivmod')
add_in_list('arm', 'Ldiv', '__aeabi_ldivmod')
add_in_list('arm', 'Lmod', '__aeabi_ldivmod')
add_in_list('arm', 'Lmul', 'art_quick_mul_long')
add_in_list('arm', 'ShlLong', 'art_quick_shl_long')
add_in_list('arm', 'ShrLong', 'art_quick_shr_long')
add_in_list('arm', 'UshrLong', 'art_quick_ushr_long')
add_in_list('arm', 'Fmod', 'fmod')
add_in_list('arm', 'Fmodf', 'fmodf')
add_in_list('arm', 'D2l', 'art_d2l')
add_in_list('arm', 'F2l', 'art_f2l')
add_in_list('arm', 'L2f', 'art_l2f')
add_in_list('arm', 'Fmod', 'art_quick_fmod')
add_in_list('arm', 'Fmodf', 'art_quick_fmodf')
add_in_list('arm', 'D2l', 'art_quick_d2l')
add_in_list('arm', 'F2l', 'art_quick_f2l')
add_in_list('arm', 'L2f', 'art_quick_l2f')
add_in_list('arm', 'Cos', 'cos')
add_in_list('arm', 'Sin', 'sin')
add_in_list('arm', 'Acos', 'acos')
add_in_list('arm', 'Asin', 'asin')
add_in_list('arm', 'Atan', 'atan')
add_in_list('arm', 'Atan2', 'atan2')
add_in_list('arm', 'Cbrt', 'cbrt')
add_in_list('arm', 'Cosh', 'cosh')
add_in_list('arm', 'Exp', 'exp')
add_in_list('arm', 'Expm1', 'expm1')
add_in_list('arm', 'Hypot', 'hypot')
add_in_list('arm', 'Log', 'log')
add_in_list('arm', 'Log10', 'log10')
add_in_list('arm', 'NextAfter', 'nextafter')
add_in_list('arm', 'Sinh', 'sinh')
add_in_list('arm', 'Tan', 'tan')
add_in_list('arm', 'Tanh', 'tanh')
add_in_list('arm', 'IndexOf', 'art_quick_indexof')
add_in_list('arm', 'StringCompareTo', 'art_quick_string_compareto')
add_in_list('arm', 'Memcpy', 'memcpy')
add_in_list('arm', 'ReadBarrierJni', 'ReadBarrierJni')
add_in_list('arm', 'ReadBarrierMark', 'artReadBarrierMark')
add_in_list('arm', 'ReadBarrierSlow', 'artReadBarrierSlow')
add_in_list('arm', 'ReadBarrierForRootSlow', 'artReadBarrierForRootSlow')
add_in_list('x86', 'InstanceofNonTrivial', 'art_quick_is_assignable')
add_in_list('x86', 'CheckCast', 'art_quick_check_cast')
add_in_list('x86', 'Cos', 'cos')
add_in_list('x86', 'Sin', 'sin')
add_in_list('x86', 'Acos', 'acos')
add_in_list('x86', 'Asin', 'asin')
add_in_list('x86', 'Atan', 'atan')
add_in_list('x86', 'Atan2', 'atan2')
add_in_list('x86', 'Cbrt', 'cbrt')
add_in_list('x86', 'Cosh', 'cosh')
add_in_list('x86', 'Exp', 'exp')
add_in_list('x86', 'Expm1', 'expm1')
add_in_list('x86', 'Hypot', 'hypot')
add_in_list('x86', 'Log', 'log')
add_in_list('x86', 'Log10', 'log10')
add_in_list('x86', 'NextAfter', 'nextafter')
add_in_list('x86', 'Sinh', 'sinh')
add_in_list('x86', 'Tan', 'tan')
add_in_list('x86', 'Tanh', 'tanh')
add_in_list('x86', 'D2l', 'art_quick_d2l')
add_in_list('x86', 'F2l', 'art_quick_f2l')
add_in_list('x86', 'Ldiv', 'art_quick_ldiv')
add_in_list('x86', 'Lmod', 'art_quick_lmod')
add_in_list('x86', 'Lmul', 'art_quick_lmul')
add_in_list('x86', 'ShlLong', 'art_quick_lshl')
add_in_list('x86', 'ShrLong', 'art_quick_lshr')
add_in_list('x86', 'UshrLong', 'art_quick_lushr')
add_in_list('x86', 'StringCompareTo', 'art_quick_string_compareto')
add_in_list('x86', 'Memcpy', 'art_quick_memcpy')
add_in_list('x86', 'ReadBarrierJni', 'ReadBarrierJni')
add_in_list('x86', 'ReadBarrierMark', 'art_quick_read_barrier_mark')
add_in_list('x86', 'ReadBarrierSlow', 'art_quick_read_barrier_slow')
add_in_list('x86', 'ReadBarrierForRootSlow', 'art_quick_read_barrier_for_root_slow')
add_in_list('mips64', 'InstanceofNonTrivial', 'artIsAssignableFromCode')
add_in_list('mips64', 'CheckCast', 'art_quick_check_cast')
add_in_list('mips64', 'CmpgDouble', 'CmpgDouble')
add_in_list('mips64', 'CmpgFloat', 'CmpgFloat')
add_in_list('mips64', 'CmplDouble', 'CmplDouble')
add_in_list('mips64', 'CmplFloat', 'CmplFloat')
add_in_list('mips64', 'Fmod', 'fmod')
add_in_list('mips64', 'L2d', 'art_l2d')
add_in_list('mips64', 'Fmodf', 'fmodf')
add_in_list('mips64', 'L2f', 'art_l2f')
add_in_list('mips64', 'D2iz', 'art_d2i')
add_in_list('mips64', 'F2iz', 'art_f2i')
add_in_list('mips64', 'D2l', 'art_d2l')
add_in_list('mips64', 'F2l', 'art_f2l')
add_in_list('mips64', 'Ldiv', 'artLdiv')
add_in_list('mips64', 'Lmod', 'artLmod')
add_in_list('mips64', 'Lmul', 'artLmul')
add_in_list('mips64', 'IndexOf', 'art_quick_indexof')
add_in_list('mips64', 'StringCompareTo', 'art_quick_string_compareto')
add_in_list('mips64', 'Memcpy', 'memcpy')
add_in_list('mips64', 'A64Load', 'Read64')
add_in_list('mips64', 'A64Store', 'Write64')
add_in_list('mips64', 'ReadBarrierJni', 'ReadBarrierJni')
add_in_list('mips64', 'ReadBarrierMark', 'artReadBarrierMark')
add_in_list('mips64', 'ReadBarrierSlow', 'artReadBarrierSlow')
add_in_list('mips64', 'ReadBarrierForRootSlow', 'artReadBarrierForRootSlow')
add_in_list('mips', 'InstanceofNonTrivial', 'artIsAssignableFromCode')
add_in_list('mips', 'CheckCast', 'art_quick_check_cast')
add_in_list('mips', 'InitializeStaticStorage', 'art_quick_initialize_static_storage')
add_in_list('mips', 'InitializeTypeAndVerifyAccess', 'art_quick_initialize_type_and_verify_access')
add_in_list('mips', 'InitializeType', 'art_quick_initialize_type')
add_in_list('mips', 'ResolveString', 'art_quick_resolve_string')
add_in_list('mips', 'Set8Instance', 'art_quick_set8_instance')
add_in_list('mips', 'Set8Static', 'art_quick_set8_static')
add_in_list('mips', 'Set16Instance', 'art_quick_set16_instance')
add_in_list('mips', 'Set16Static', 'art_quick_set16_static')
add_in_list('mips', 'Set32Instance', 'art_quick_set32_instance')
add_in_list('mips', 'Set32Static', 'art_quick_set32_static')
add_in_list('mips', 'Set64Instance', 'art_quick_set64_instance')
add_in_list('mips', 'Set64Static', 'art_quick_set64_static')
add_in_list('mips', 'SetObjInstance', 'art_quick_set_obj_instance')
add_in_list('mips', 'SetObjStatic', 'art_quick_set_obj_static')
add_in_list('mips', 'GetBooleanInstance', 'art_quick_get_boolean_instance')
add_in_list('mips', 'GetByteInstance', 'art_quick_get_byte_instance')
add_in_list('mips', 'GetCharInstance', 'art_quick_get_char_instance')
add_in_list('mips', 'GetShortInstance', 'art_quick_get_short_instance')
add_in_list('mips', 'Get32Instance', 'art_quick_get32_instance')
add_in_list('mips', 'Get64Instance', 'art_quick_get64_instance')
add_in_list('mips', 'GetObjInstance', 'art_quick_get_obj_instance')
add_in_list('mips', 'GetBooleanStatic', 'art_quick_get_boolean_static')
add_in_list('mips', 'GetByteStatic', 'art_quick_get_byte_static')
add_in_list('mips', 'GetCharStatic', 'art_quick_get_char_static')
add_in_list('mips', 'GetShortStatic', 'art_quick_get_short_static')
add_in_list('mips', 'Get32Static', 'art_quick_get32_static')
add_in_list('mips', 'Get64Static', 'art_quick_get64_static')
add_in_list('mips', 'GetObjStatic', 'art_quick_get_obj_static')
add_in_list('mips', 'AputObjectWithNullAndBoundCheck', 'art_quick_aput_obj_with_null_and_bound_check')
add_in_list('mips', 'AputObjectWithBoundCheck', 'art_quick_aput_obj_with_bound_check')
add_in_list('mips', 'AputObject', 'art_quick_aput_obj')
add_in_list('mips', 'HandleFillArrayData', 'art_quick_handle_fill_data')
add_in_list('mips', 'JniMethodStart', 'JniMethodStart')
add_in_list('mips', 'JniMethodStartSynchronized', 'JniMethodStartSynchronized')
add_in_list('mips', 'JniMethodEnd', 'JniMethodEnd')
add_in_list('mips', 'JniMethodEndSynchronized', 'JniMethodEndSynchronized')
add_in_list('mips', 'JniMethodEndWithReference', 'JniMethodEndWithReference')
add_in_list('mips', 'JniMethodEndWithReferenceSynchronized', 'JniMethodEndWithReferenceSynchronized')
add_in_list('mips', 'QuickGenericJniTrampoline', 'art_quick_generic_jni_trampoline')
add_in_list('mips', 'LockObject', 'art_quick_lock_object_no_inline')
add_in_list('mips', 'UnlockObject', 'art_quick_unlock_object_no_inline')
add_in_list('mips', 'LockObject', 'art_quick_lock_object')
add_in_list('mips', 'UnlockObject', 'art_quick_unlock_object')
add_in_list('mips', 'CmpgDouble', 'CmpgDouble')
add_in_list('mips', 'CmpgFloat', 'CmpgFloat')
add_in_list('mips', 'CmplDouble', 'CmplDouble')
add_in_list('mips', 'CmplFloat', 'CmplFloat')
add_in_list('mips', 'Fmod', 'fmod')
add_in_list('mips', 'L2d', 'art_l2d')
add_in_list('mips', 'Fmodf', 'fmodf')
add_in_list('mips', 'L2f', 'art_l2f')
add_in_list('mips', 'D2iz', 'art_d2i')
add_in_list('mips', 'F2iz', 'art_f2i')
add_in_list('mips', 'D2l', 'art_d2l')
add_in_list('mips', 'F2l', 'art_f2l')
add_in_list('mips', 'Ldiv', 'artLdiv')
add_in_list('mips', 'Lmod', 'artLmod')
add_in_list('mips', 'Lmul', 'artLmul')
add_in_list('mips', 'ShlLong', 'art_quick_shl_long')
add_in_list('mips', 'ShrLong', 'art_quick_shr_long')
add_in_list('mips', 'UshrLong', 'art_quick_ushr_long')
add_in_list('mips', 'IndexOf', 'art_quick_indexof')
add_in_list('mips', 'StringCompareTo', 'art_quick_string_compareto')
add_in_list('mips', 'Memcpy', 'memcpy')
add_in_list('mips', 'QuickImtConflictTrampoline', 'art_quick_imt_conflict_trampoline')
add_in_list('mips', 'QuickResolutionTrampoline', 'art_quick_resolution_trampoline')
add_in_list('mips', 'QuickToInterpreterBridge', 'art_quick_to_interpreter_bridge')
add_in_list('mips', 'TestSuspend', 'art_quick_test_suspend')
add_in_list('mips', 'DeliverException', 'art_quick_deliver_exception')
add_in_list('mips', 'ThrowArrayBounds', 'art_quick_throw_array_bounds')
add_in_list('mips', 'ThrowDivZero', 'art_quick_throw_div_zero')
add_in_list('mips', 'ThrowNoSuchMethod', 'art_quick_throw_no_such_method')
add_in_list('mips', 'ThrowNullPointer', 'art_quick_throw_null_pointer_exception')
add_in_list('mips', 'ThrowStackOverflow', 'art_quick_throw_stack_overflow')
add_in_list('mips', 'Deoptimize', 'art_quick_deoptimize_from_compiled_code')
add_in_list('mips', 'A64Load', 'QuasiAtomic::Read64')
add_in_list('mips', 'A64Store', 'QuasiAtomic::Write64')
add_in_list('mips', 'ReadBarrierJni', 'ReadBarrierJni')
add_in_list('mips', 'ReadBarrierMark', 'artReadBarrierMark')
add_in_list('mips', 'ReadBarrierSlow', 'artReadBarrierSlow')
add_in_list('mips', 'ReadBarrierForRootSlow', 'artReadBarrierForRootSlow')
add_in_list('x86_64', 'InstanceofNonTrivial', 'art_quick_assignable_from_code')
add_in_list('x86_64', 'CheckCast', 'art_quick_check_cast')
add_in_list('x86_64', 'Cos', 'cos')
add_in_list('x86_64', 'Sin', 'sin')
add_in_list('x86_64', 'Acos', 'acos')
add_in_list('x86_64', 'Asin', 'asin')
add_in_list('x86_64', 'Atan', 'atan')
add_in_list('x86_64', 'Atan2', 'atan2')
add_in_list('x86_64', 'Cbrt', 'cbrt')
add_in_list('x86_64', 'Cosh', 'cosh')
add_in_list('x86_64', 'Exp', 'exp')
add_in_list('x86_64', 'Expm1', 'expm1')
add_in_list('x86_64', 'Hypot', 'hypot')
add_in_list('x86_64', 'Log', 'log')
add_in_list('x86_64', 'Log10', 'log10')
add_in_list('x86_64', 'NextAfter', 'nextafter')
add_in_list('x86_64', 'Sinh', 'sinh')
add_in_list('x86_64', 'Tan', 'tan')
add_in_list('x86_64', 'Tanh', 'tanh')
add_in_list('x86_64', 'D2l', 'art_d2l')
add_in_list('x86_64', 'F2l', 'art_f2l')
add_in_list('x86_64', 'Ldiv', 'art_quick_ldiv')
add_in_list('x86_64', 'Lmod', 'art_quick_lmod')
add_in_list('x86_64', 'Lmul', 'art_quick_lmul')
add_in_list('x86_64', 'ShlLong', 'art_quick_lshl')
add_in_list('x86_64', 'ShrLong', 'art_quick_lshr')
add_in_list('x86_64', 'UshrLong', 'art_quick_lushr')
add_in_list('x86_64', 'StringCompareTo', 'art_quick_string_compareto')
add_in_list('x86_64', 'Memcpy', 'art_quick_memcpy')
add_in_list('x86_64', 'ReadBarrierJni', 'ReadBarrierJni')
add_in_list('x86_64', 'ReadBarrierMark', 'art_quick_read_barrier_mark')
add_in_list('x86_64', 'ReadBarrierSlow', 'art_quick_read_barrier_slow')
add_in_list('x86_64', 'ReadBarrierForRootSlow', 'art_quick_read_barrier_for_root_slow')
add_in_list('arm64', 'InstanceofNonTrivial', 'artIsAssignableFromCode')
add_in_list('arm64', 'CheckCast', 'art_quick_check_cast')
add_in_list('arm64', 'Fmod', 'fmod')
add_in_list('arm64', 'Fmodf', 'fmodf')
add_in_list('arm64', 'Cos', 'cos')
add_in_list('arm64', 'Sin', 'sin')
add_in_list('arm64', 'Acos', 'acos')
add_in_list('arm64', 'Asin', 'asin')
add_in_list('arm64', 'Atan', 'atan')
add_in_list('arm64', 'Atan2', 'atan2')
add_in_list('arm64', 'Cbrt', 'cbrt')
add_in_list('arm64', 'Cosh', 'cosh')
add_in_list('arm64', 'Exp', 'exp')
add_in_list('arm64', 'Expm1', 'expm1')
add_in_list('arm64', 'Hypot', 'hypot')
add_in_list('arm64', 'Log', 'log')
add_in_list('arm64', 'Log10', 'log10')
add_in_list('arm64', 'NextAfter', 'nextafter')
add_in_list('arm64', 'Sinh', 'sinh')
add_in_list('arm64', 'Tan', 'tan')
add_in_list('arm64', 'Tanh', 'tanh')
add_in_list('arm64', 'IndexOf', 'art_quick_indexof')
add_in_list('arm64', 'StringCompareTo', 'art_quick_string_compareto')
add_in_list('arm64', 'Memcpy', 'memcpy')
add_in_list('arm64', 'ReadBarrierJni', 'ReadBarrierJni')
add_in_list('arm64', 'ReadBarrierMark', 'artReadBarrierMark')
add_in_list('arm64', 'ReadBarrierSlow', 'artReadBarrierSlow')
add_in_list('arm64', 'ReadBarrierForRootSlow', 'artReadBarrierForRootSlow')

### END OF COMMAND DUMP ###

# The following instructions are generated using:
# grep V quick_entrypoints_list.h | sed "s/ *V(/args_mapping['/" | sed "s/, /'] = ['/" | sed "s/, /', '/g" | sed "s/) \\\\/']/"
# in the art/runtime/entrypoints/quick/ directory

args_mapping['AllocArray'] = ['void*', 'uint32_t', 'int32_t', 'ArtMethod*']
args_mapping['AllocArrayResolved'] = ['void*', 'mirror::Class*', 'int32_t', 'ArtMethod*']
args_mapping['AllocArrayWithAccessCheck'] = ['void*', 'uint32_t', 'int32_t', 'ArtMethod*']
args_mapping['AllocObject'] = ['void*', 'uint32_t', 'ArtMethod*']
args_mapping['AllocObjectResolved'] = ['void*', 'mirror::Class*', 'ArtMethod*']
args_mapping['AllocObjectInitialized'] = ['void*', 'mirror::Class*', 'ArtMethod*']
args_mapping['AllocObjectWithAccessCheck'] = ['void*', 'uint32_t', 'ArtMethod*']
args_mapping['CheckAndAllocArray'] = ['void*', 'uint32_t', 'int32_t', 'ArtMethod*']
args_mapping['CheckAndAllocArrayWithAccessCheck'] = ['void*', 'uint32_t', 'int32_t', 'ArtMethod*']
args_mapping['AllocStringFromBytes'] = ['void*', 'void*', 'int32_t', 'int32_t', 'int32_t']
args_mapping['AllocStringFromChars'] = ['void*', 'int32_t', 'int32_t', 'void*']
args_mapping['AllocStringFromString'] = ['void*', 'void*']
args_mapping['InstanceofNonTrivial'] = ['uint32_t', 'const mirror::Class*', 'const mirror::Class*']
args_mapping['CheckCast'] = ['void', 'const mirror::Class*', 'const mirror::Class*']
args_mapping['InitializeStaticStorage'] = ['void*', 'uint32_t']
args_mapping['InitializeTypeAndVerifyAccess'] = ['void*', 'uint32_t']
args_mapping['InitializeType'] = ['void*', 'uint32_t']
args_mapping['ResolveString'] = ['void*', 'uint32_t']
args_mapping['Set8Instance'] = ['int', 'uint32_t', 'void*', 'int8_t']
args_mapping['Set8Static'] = ['int', 'uint32_t', 'int8_t']
args_mapping['Set16Instance'] = ['int', 'uint32_t', 'void*', 'int16_t']
args_mapping['Set16Static'] = ['int', 'uint32_t', 'int16_t']
args_mapping['Set32Instance'] = ['int', 'uint32_t', 'void*', 'int32_t']
args_mapping['Set32Static'] = ['int', 'uint32_t', 'int32_t']
args_mapping['Set64Instance'] = ['int', 'uint32_t', 'void*', 'int64_t']
args_mapping['Set64Static'] = ['int', 'uint32_t', 'int64_t']
args_mapping['SetObjInstance'] = ['int', 'uint32_t', 'void*', 'void*']
args_mapping['SetObjStatic'] = ['int', 'uint32_t', 'void*']
args_mapping['GetByteInstance'] = ['int8_t', 'uint32_t', 'void*']
args_mapping['GetBooleanInstance'] = ['uint8_t', 'uint32_t', 'void*']
args_mapping['GetByteStatic'] = ['int8_t', 'uint32_t']
args_mapping['GetBooleanStatic'] = ['uint8_t', 'uint32_t']
args_mapping['GetShortInstance'] = ['int16_t', 'uint32_t', 'void*']
args_mapping['GetCharInstance'] = ['uint16_t', 'uint32_t', 'void*']
args_mapping['GetShortStatic'] = ['int16_t', 'uint32_t']
args_mapping['GetCharStatic'] = ['uint16_t', 'uint32_t']
args_mapping['Get32Instance'] = ['int32_t', 'uint32_t', 'void*']
args_mapping['Get32Static'] = ['int32_t', 'uint32_t']
args_mapping['Get64Instance'] = ['int64_t', 'uint32_t', 'void*']
args_mapping['Get64Static'] = ['int64_t', 'uint32_t']
args_mapping['GetObjInstance'] = ['void*', 'uint32_t', 'void*']
args_mapping['GetObjStatic'] = ['void*', 'uint32_t']
args_mapping['AputObjectWithNullAndBoundCheck'] = ['void', 'mirror::Array*', 'int32_t', 'mirror::Object*']
args_mapping['AputObjectWithBoundCheck'] = ['void', 'mirror::Array*', 'int32_t', 'mirror::Object*']
args_mapping['AputObject'] = ['void', 'mirror::Array*', 'int32_t', 'mirror::Object*']
args_mapping['HandleFillArrayData'] = ['void', 'void*', 'void*']
args_mapping['JniMethodStart'] = ['uint32_t', 'Thread*']
args_mapping['JniMethodStartSynchronized'] = ['uint32_t', 'jobject', 'Thread*']
args_mapping['JniMethodEnd'] = ['void', 'uint32_t', 'Thread*']
args_mapping['JniMethodEndSynchronized'] = ['void', 'uint32_t', 'jobject', 'Thread*']
args_mapping['JniMethodEndWithReference'] = ['mirror::Object*', 'jobject', 'uint32_t', 'Thread*']
args_mapping['JniMethodEndWithReferenceSynchronized'] = ['mirror::Object*', 'jobject', 'uint32_t', 'jobject', 'Thread*']
args_mapping['QuickGenericJniTrampoline'] = ['void', 'ArtMethod*']
args_mapping['LockObject'] = ['void', 'mirror::Object*']
args_mapping['UnlockObject'] = ['void', 'mirror::Object*']
args_mapping['CmpgDouble'] = ['int32_t', 'double', 'double']
args_mapping['CmpgFloat'] = ['int32_t', 'float', 'float']
args_mapping['CmplDouble'] = ['int32_t', 'double', 'double']
args_mapping['CmplFloat'] = ['int32_t', 'float', 'float']
args_mapping['Cos'] = ['double', 'double']
args_mapping['Sin'] = ['double', 'double']
args_mapping['Acos'] = ['double', 'double']
args_mapping['Asin'] = ['double', 'double']
args_mapping['Atan'] = ['double', 'double']
args_mapping['Atan2'] = ['double', 'double', 'double']
args_mapping['Cbrt'] = ['double', 'double']
args_mapping['Cosh'] = ['double', 'double']
args_mapping['Exp'] = ['double', 'double']
args_mapping['Expm1'] = ['double', 'double']
args_mapping['Hypot'] = ['double', 'double', 'double']
args_mapping['Log'] = ['double', 'double']
args_mapping['Log10'] = ['double', 'double']
args_mapping['NextAfter'] = ['double', 'double', 'double']
args_mapping['Sinh'] = ['double', 'double']
args_mapping['Tan'] = ['double', 'double']
args_mapping['Tanh'] = ['double', 'double']
args_mapping['Fmod'] = ['double', 'double', 'double']
args_mapping['L2d'] = ['double', 'int64_t']
args_mapping['Fmodf'] = ['float', 'float', 'float']
args_mapping['L2f'] = ['float', 'int64_t']
args_mapping['D2iz'] = ['int32_t', 'double']
args_mapping['F2iz'] = ['int32_t', 'float']
args_mapping['Idivmod'] = ['int32_t', 'int32_t', 'int32_t']
args_mapping['D2l'] = ['int64_t', 'double']
args_mapping['F2l'] = ['int64_t', 'float']
args_mapping['Ldiv'] = ['int64_t', 'int64_t', 'int64_t']
args_mapping['Lmod'] = ['int64_t', 'int64_t', 'int64_t']
args_mapping['Lmul'] = ['int64_t', 'int64_t', 'int64_t']
args_mapping['ShlLong'] = ['uint64_t', 'uint64_t', 'uint32_t']
args_mapping['ShrLong'] = ['uint64_t', 'uint64_t', 'uint32_t']
args_mapping['UshrLong'] = ['uint64_t', 'uint64_t', 'uint32_t']
args_mapping['IndexOf'] = ['int32_t', 'void*', 'uint32_t', 'uint32_t']
args_mapping['StringCompareTo'] = ['int32_t', 'void*', 'void*']
args_mapping['Memcpy'] = ['void*', 'void*', 'const void*', 'size_t']
args_mapping['QuickImtConflictTrampoline'] = ['void', 'ArtMethod*']
args_mapping['QuickResolutionTrampoline'] = ['void', 'ArtMethod*']
args_mapping['QuickToInterpreterBridge'] = ['void', 'ArtMethod*']
args_mapping['InvokeDirectTrampolineWithAccessCheck'] = ['void', 'uint32_t', 'void*']
args_mapping['InvokeInterfaceTrampolineWithAccessCheck'] = ['void', 'uint32_t', 'void*']
args_mapping['InvokeStaticTrampolineWithAccessCheck'] = ['void', 'uint32_t', 'void*']
args_mapping['InvokeSuperTrampolineWithAccessCheck'] = ['void', 'uint32_t', 'void*']
args_mapping['InvokeVirtualTrampolineWithAccessCheck'] = ['void', 'uint32_t', 'void*']
args_mapping['TestSuspend'] = ['void', 'void']
args_mapping['DeliverException'] = ['void', 'mirror::Object*']
args_mapping['ThrowArrayBounds'] = ['void', 'int32_t', 'int32_t']
args_mapping['ThrowDivZero'] = ['void', 'void']
args_mapping['ThrowNoSuchMethod'] = ['void', 'int32_t']
args_mapping['ThrowNullPointer'] = ['void', 'void']
args_mapping['ThrowStackOverflow'] = ['void', 'void*']
args_mapping['Deoptimize'] = ['void', 'void']
args_mapping['A64Load'] = ['int64_t', 'volatile const int64_t *']
args_mapping['A64Store'] = ['void', 'volatile int64_t *', 'int64_t']
args_mapping['NewEmptyString'] = ['void']
args_mapping['NewStringFromBytes_B'] = ['void']
args_mapping['NewStringFromBytes_BI'] = ['void']
args_mapping['NewStringFromBytes_BII'] = ['void']
args_mapping['NewStringFromBytes_BIII'] = ['void']
args_mapping['NewStringFromBytes_BIIString'] = ['void']
args_mapping['NewStringFromBytes_BString'] = ['void']
args_mapping['NewStringFromBytes_BIICharset'] = ['void']
args_mapping['NewStringFromBytes_BCharset'] = ['void']
args_mapping['NewStringFromChars_C'] = ['void']
args_mapping['NewStringFromChars_CII'] = ['void']
args_mapping['NewStringFromChars_IIC'] = ['void']
args_mapping['NewStringFromCodePoints'] = ['void']
args_mapping['NewStringFromString'] = ['void']
args_mapping['NewStringFromStringBuffer'] = ['void']
args_mapping['NewStringFromStringBuilder'] = ['void']
args_mapping['ReadBarrierJni'] = ['void', 'mirror::CompressedReference<mirror::Object>*', 'Thread*']
args_mapping['ReadBarrierMark'] = ['mirror::Object*', 'mirror::Object*']
args_mapping['ReadBarrierSlow'] = ['mirror::Object*', 'mirror::Object*', 'mirror::Object*', 'uint32_t']
args_mapping['ReadBarrierForRootSlow'] = ['mirror::Object*', 'GcRoot<mirror::Object>*']

### END OF COMMAND DUMP ###


with open("../entrypoints/quick/wrap_quick_entrypoints.h", "w+") as f_header:
    print >>f_header, '''\
/* Generated by generate_wrapper.py */

#include <jni.h>
#include <stddef.h>

#include "mirror/class-inl.h"
#include "runtime.h"

namespace art {
'''
    for arch, function_mapping in [('arm', function_mapping_arm),
                      ('arm64', function_mapping_arm64),
                      ('x86', function_mapping_x86),
                      ('x86_64', function_mapping_x86_64),
                      ('mips', function_mapping_mips),
                      ('mips64', function_mapping_mips64)]:
        with open(arch+"/wrap_quick_entrypoints.S", "w+") as f:

            print >>f, '''\
/* Generated by generate_wrapper.py */

#include "asm_support_%s.S"
''' % (arch)
            if arch == 'arm64':
                print >>f, '''\
    /* void do_wrap_enter(void* lr);
     * Call UntrackEnter(true) and save the lr
     */
    .extern do_wrap_enter
    
    
    /* void* do_wrap_exit();
     * x0 contains the result
     * Call UntrackExit(true) and return old_lr value
     */
    .extern do_wrap_exit

.macro SAVE_ALL_REGS
    sub sp, sp, #0x1f0
    stp x0, x1, [sp]
    stp x2, x3, [sp, #16]
    stp x4, x5, [sp, #32]
    stp x6, x7, [sp, #48]
    stp x8, x9, [sp, #64]
    stp x10, x11, [sp, #80]
    stp x12, x13, [sp, #96]
    stp x14, x15, [sp, #112]
    stp x16, x17, [sp, #128]
    stp x18, x19, [sp, #144]
    stp x20, x21, [sp, #160]
    stp x22, x23, [sp, #176]
    stp x24, x25, [sp, #192]
    stp x26, x27, [sp, #208]
    stp x28, x29, [sp, #224]
    stp d0, d1, [sp, #240]
    stp d2, d3, [sp, #256]
    stp d4, d5, [sp, #272]
    stp d6, d7, [sp, #288]
    stp d8, d9, [sp, #304]
    stp d10, d11, [sp, #320]
    stp d12, d13, [sp, #336]
    stp d14, d15, [sp, #352]
    stp d16, d17, [sp, #368]
    stp d18, d19, [sp, #384]
    stp d20, d21, [sp, #400]
    stp d22, d23, [sp, #416]
    stp d24, d25, [sp, #432]
    stp d26, d27, [sp, #448]
    stp d28, d29, [sp, #464]
    stp d30, d31, [sp, #480]
.endm

.macro RESTORE_ALL_REGS
    ldp x0, x1, [sp]
    ldp x2, x3, [sp, #16]
    ldp x4, x5, [sp, #32]
    ldp x6, x7, [sp, #48]
    ldp x8, x9, [sp, #64]
    ldp x10, x11, [sp, #80]
    ldp x12, x13, [sp, #96]
    ldp x14, x15, [sp, #112]
    ldp x16, x17, [sp, #128]
    ldp x18, x19, [sp, #144]
    ldp x20, x21, [sp, #160]
    ldp x22, x23, [sp, #176]
    ldp x24, x25, [sp, #192]
    ldp x26, x27, [sp, #208]
    ldp x28, x29, [sp, #224]
    ldp d0, d1, [sp, #240]
    ldp d2, d3, [sp, #256]
    ldp d4, d5, [sp, #272]
    ldp d6, d7, [sp, #288]
    ldp d8, d9, [sp, #304]
    ldp d10, d11, [sp, #320]
    ldp d12, d13, [sp, #336]
    ldp d14, d15, [sp, #352]
    ldp d16, d17, [sp, #368]
    ldp d18, d19, [sp, #384]
    ldp d20, d21, [sp, #400]
    ldp d22, d23, [sp, #416]
    ldp d24, d25, [sp, #432]
    ldp d26, d27, [sp, #448]
    ldp d28, d29, [sp, #464]
    ldp d30, d31, [sp, #480]
    add sp, sp, #0x1f0
.endm

.macro WRAP_FUNCTION func_name
    .extern \\func_name
    ENTRY wrap_\\func_name
        SAVE_ALL_REGS
        mov x0, xLR
        bl do_wrap_enter
        RESTORE_ALL_REGS
        adr xLR, wrap_exit
        b \\func_name
    END wrap_\\func_name
.endm

ENTRY wrap_exit
    SAVE_ALL_REGS
    bl do_wrap_exit
    mov xLR, x0
    RESTORE_ALL_REGS
    ret
END wrap_exit

'''
                alreadyDone = []
                for name in function_mapping:
                    for function in function_mapping[name]:
                        if function not in alreadyDone:
                            ns = ""
                            if "QuasiAtomic::" in function:
                                function = function[len("QuasiAtomic::"):]
                                ns = "QuasiAtomic::"

                            print >>f, "WRAP_FUNCTION %s" % (function)
                            alreadyDone.append(function)

                print >>f, '''\

#undef WRAP_FUNCTION'''


            elif arch == 'arm':
                print >>f, '''\
    /* void do_wrap_enter(void* lr);
     * Call UntrackEnter(true) and save the lr
     */
    .extern do_wrap_enter


    /* void* do_wrap_exit();
     * x0 contains the result
     * Call UntrackExit(true) and return old_lr value
     */
    .extern do_wrap_exit

.macro SAVE_ALL_REGS
    stmdb sp!, {r0, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11}
.endm

.macro RESTORE_ALL_REGS
    ldmia sp!, {r0, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11}
.endm

.macro WRAP_FUNCTION func_name

    ARM_ENTRY wrap_exit_\\func_name
        SAVE_ALL_REGS
        blx do_wrap_exit
        mov lr, r0
        RESTORE_ALL_REGS
        mov pc, lr
    END wrap_exit_\\func_name

        .extern \\func_name
    ENTRY wrap_\\func_name
        SAVE_ALL_REGS
        mov r0, lr
        blx do_wrap_enter
        RESTORE_ALL_REGS
        adr lr, wrap_exit_\\func_name
        b \\func_name
    END wrap_\\func_name
.endm

'''
                alreadyDone = []
                for name in function_mapping:
                    for function in function_mapping[name]:
                        if function not in alreadyDone:
                            ns = ""
                            if "QuasiAtomic::" in function:
                                function = function[len("QuasiAtomic::"):]
                                ns = "QuasiAtomic::"

                            print >>f, "WRAP_FUNCTION %s" % (function)
                            alreadyDone.append(function)

                print >>f, '''\

#undef WRAP_FUNCTION'''

            elif arch == 'x86' or arch == 'x86_64':
                alreadyDone = []
                for name in function_mapping:
                    for function in function_mapping[name]:
                        if function not in alreadyDone:
                            ns = ""
                            if "QuasiAtomic::" in function:
                                function = function[len("QuasiAtomic::"):]
                                ns = "QuasiAtomic::"

                            print >>f, "DEFINE_FUNCTION wrap_%s\nEND_FUNCTION wrap_%s" % (function, function)
                            alreadyDone.append(function)
            else:
                alreadyDone = []
                for name in function_mapping:
                    for function in function_mapping[name]:
                        if function not in alreadyDone:
                            ns = ""
                            if "QuasiAtomic::" in function:
                                function = function[len("QuasiAtomic::"):]
                                ns = "QuasiAtomic::"

                            print >>f, "ENTRY wrap_%s\nEND wrap_%s" % (function, function)
                            alreadyDone.append(function)



            print >>f_header, "\n#if defined(%s)" % ("__arm__" if arch == 'arm'
                                                   else "__aarch64__" if arch == 'arm64'
                                                   else "__i386__" if arch == 'x86'
                                                   else "__x86_64__" if arch == 'x86_64'
                                                   else "__mips__" if arch == 'mips'
                                                   else "__mips__" if arch == 'mips64'
                                                   else "")

            alreadyDone = []
            for name in function_mapping:
                for function in function_mapping[name]:
                    if function not in alreadyDone:
                        ret_type = args_mapping[name][0]
                        args = args_mapping[name][1:]
                        args = [] if args[0] == 'void' else args

                        sig = ', '.join([v + " " + chr(ord('a')+i) for i, v in enumerate(args, 0)])
                        call = ', '.join([chr(ord('a')+i) for i in xrange(len(args))])

                        if "QuasiAtomic::" in function:
                            function = function[len("QuasiAtomic::"):]

                        print >>f_header, "extern \"C\" %s wrap_%s(%s);" % (ret_type, function, sig)
                        alreadyDone.append(function)
            print >>f_header, "#endif"



    print >>f_header,'''
}
'''
