/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "entrypoints/jni/jni_entrypoints.h"
#include "entrypoints/quick/quick_alloc_entrypoints.h"
#include "entrypoints/quick/quick_default_externs.h"
#include "entrypoints/quick/quick_default_init_entrypoints.h"
#include "entrypoints/quick/quick_entrypoints.h"
#include "entrypoints/quick/wrap_quick_entrypoints.h"
#include "entrypoints/entrypoint_utils.h"
#include "entrypoints/math_entrypoints.h"
#include "entrypoints/runtime_asm_entrypoints.h"
#include "interpreter/interpreter.h"

namespace art {

// Cast entrypoints.
extern "C" uint32_t artIsAssignableFromCode(const mirror::Class* klass,
                                            const mirror::Class* ref_class);

void InitEntryPoints(JniEntryPoints* jpoints, QuickEntryPoints* qpoints) {
  DefaultInitEntryPoints(jpoints, qpoints);

  // Cast
  qpoints->pInstanceofNonTrivial = wrap_artIsAssignableFromCode;
  qpoints->pCheckCast = wrap_art_quick_check_cast;

  // Math
  // TODO null entrypoints not needed for ARM64 - generate inline.
  qpoints->pCmpgDouble = nullptr;
  qpoints->pCmpgFloat = nullptr;
  qpoints->pCmplDouble = nullptr;
  qpoints->pCmplFloat = nullptr;
  qpoints->pFmod = wrap_fmod;
  qpoints->pL2d = nullptr;
  qpoints->pFmodf = wrap_fmodf;
  qpoints->pL2f = nullptr;
  qpoints->pD2iz = nullptr;
  qpoints->pF2iz = nullptr;
  qpoints->pIdivmod = nullptr;
  qpoints->pD2l = nullptr;
  qpoints->pF2l = nullptr;
  qpoints->pLdiv = nullptr;
  qpoints->pLmod = nullptr;
  qpoints->pLmul = nullptr;
  qpoints->pShlLong = nullptr;
  qpoints->pShrLong = nullptr;
  qpoints->pUshrLong = nullptr;

  // More math.
  qpoints->pCos = wrap_cos;
  qpoints->pSin = wrap_sin;
  qpoints->pAcos = wrap_acos;
  qpoints->pAsin = wrap_asin;
  qpoints->pAtan = wrap_atan;
  qpoints->pAtan2 = wrap_atan2;
  qpoints->pCbrt = wrap_cbrt;
  qpoints->pCosh = wrap_cosh;
  qpoints->pExp = wrap_exp;
  qpoints->pExpm1 = wrap_expm1;
  qpoints->pHypot = wrap_hypot;
  qpoints->pLog = wrap_log;
  qpoints->pLog10 = wrap_log10;
  qpoints->pNextAfter = wrap_nextafter;
  qpoints->pSinh = wrap_sinh;
  qpoints->pTan = wrap_tan;
  qpoints->pTanh = wrap_tanh;

  // Intrinsics
  qpoints->pIndexOf = wrap_art_quick_indexof;
  qpoints->pStringCompareTo = wrap_art_quick_string_compareto;
  qpoints->pMemcpy = wrap_memcpy;

  // Read barrier.
  qpoints->pReadBarrierJni = wrap_ReadBarrierJni;
  qpoints->pReadBarrierMark = wrap_artReadBarrierMark;
  qpoints->pReadBarrierSlow = wrap_artReadBarrierSlow;
  qpoints->pReadBarrierForRootSlow = wrap_artReadBarrierForRootSlow;
};

}  // namespace art
