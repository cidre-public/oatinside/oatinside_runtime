/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "entrypoints/jni/jni_entrypoints.h"
#include "entrypoints/quick/quick_alloc_entrypoints.h"
#include "entrypoints/quick/quick_default_externs.h"
#include "entrypoints/quick/quick_default_init_entrypoints.h"
#include "entrypoints/quick/quick_entrypoints.h"
#include "entrypoints/quick/wrap_quick_entrypoints.h"
#include "entrypoints/entrypoint_utils.h"
#include "entrypoints/math_entrypoints.h"
#include "entrypoints/runtime_asm_entrypoints.h"
#include "interpreter/interpreter.h"

namespace art {

// Cast entrypoints.
extern "C" uint32_t artIsAssignableFromCode(const mirror::Class* klass,
                                            const mirror::Class* ref_class);


// Used by soft float.
// Single-precision FP arithmetics.
extern "C" float fmodf(float a, float b);              // REM_FLOAT[_2ADDR]
// Double-precision FP arithmetics.
extern "C" double fmod(double a, double b);            // REM_DOUBLE[_2ADDR]

// Used by hard float.
extern "C" float art_quick_fmodf(float a, float b);    // REM_FLOAT[_2ADDR]
extern "C" double art_quick_fmod(double a, double b);  // REM_DOUBLE[_2ADDR]

// Integer arithmetics.
extern "C" int __aeabi_idivmod(int32_t, int32_t);  // [DIV|REM]_INT[_2ADDR|_LIT8|_LIT16]

// Long long arithmetics - REM_LONG[_2ADDR] and DIV_LONG[_2ADDR]
extern "C" int64_t __aeabi_ldivmod(int64_t, int64_t);

void InitEntryPoints(JniEntryPoints* jpoints, QuickEntryPoints* qpoints) {
  DefaultInitEntryPoints(jpoints, qpoints);

  // Cast
  qpoints->pInstanceofNonTrivial = wrap_artIsAssignableFromCode;
  qpoints->pCheckCast = wrap_art_quick_check_cast;

  // Math
  qpoints->pIdivmod = wrap___aeabi_idivmod;
  qpoints->pLdiv = wrap___aeabi_ldivmod;
  qpoints->pLmod = wrap___aeabi_ldivmod;  // result returned in r2:r3
  qpoints->pLmul = wrap_art_quick_mul_long;
  qpoints->pShlLong = wrap_art_quick_shl_long;
  qpoints->pShrLong = wrap_art_quick_shr_long;
  qpoints->pUshrLong = wrap_art_quick_ushr_long;
  if (kArm32QuickCodeUseSoftFloat) {
    qpoints->pFmod = wrap_fmod;
    qpoints->pFmodf = wrap_fmodf;
    qpoints->pD2l = wrap_art_d2l;
    qpoints->pF2l = wrap_art_f2l;
    qpoints->pL2f = wrap_art_l2f;
  } else {
    qpoints->pFmod = wrap_art_quick_fmod;
    qpoints->pFmodf = wrap_art_quick_fmodf;
    qpoints->pD2l = wrap_art_quick_d2l;
    qpoints->pF2l = wrap_art_quick_f2l;
    qpoints->pL2f = wrap_art_quick_l2f;
  }

  // More math.
  qpoints->pCos = wrap_cos;
  qpoints->pSin = wrap_sin;
  qpoints->pAcos = wrap_acos;
  qpoints->pAsin = wrap_asin;
  qpoints->pAtan = wrap_atan;
  qpoints->pAtan2 = wrap_atan2;
  qpoints->pCbrt = wrap_cbrt;
  qpoints->pCosh = wrap_cosh;
  qpoints->pExp = wrap_exp;
  qpoints->pExpm1 = wrap_expm1;
  qpoints->pHypot = wrap_hypot;
  qpoints->pLog = wrap_log;
  qpoints->pLog10 = wrap_log10;
  qpoints->pNextAfter = wrap_nextafter;
  qpoints->pSinh = wrap_sinh;
  qpoints->pTan = wrap_tan;
  qpoints->pTanh = wrap_tanh;

  // Intrinsics
  qpoints->pIndexOf = wrap_art_quick_indexof;
  qpoints->pStringCompareTo = wrap_art_quick_string_compareto;
  qpoints->pMemcpy = wrap_memcpy;

  // Read barrier.
  qpoints->pReadBarrierJni = wrap_ReadBarrierJni;
  qpoints->pReadBarrierMark = wrap_artReadBarrierMark;
  qpoints->pReadBarrierSlow = wrap_artReadBarrierSlow;
  qpoints->pReadBarrierForRootSlow = wrap_artReadBarrierForRootSlow;
}

}  // namespace art
