/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ART_RUNTIME_ENTRYPOINTS_QUICK_QUICK_DEFAULT_INIT_ENTRYPOINTS_H_
#define ART_RUNTIME_ENTRYPOINTS_QUICK_QUICK_DEFAULT_INIT_ENTRYPOINTS_H_

#include "base/logging.h"
#include "entrypoints/jni/jni_entrypoints.h"
#include "entrypoints/quick/wrap_quick_entrypoints.h"
#include "entrypoints/runtime_asm_entrypoints.h"
#include "quick_alloc_entrypoints.h"
#include "quick_default_externs.h"
#include "quick_entrypoints.h"

// NOTE: All wrap_* func are automatically untracked using wrappers (assembly macro)
// All other are manually untracked (ie Untrack functions are directly put into the function body)

namespace art {

void DefaultInitEntryPoints(JniEntryPoints* jpoints, QuickEntryPoints* qpoints) {
  // JNI
  jpoints->pDlsymLookup = art_jni_dlsym_lookup_stub;

  // Alloc
  ResetQuickAllocEntryPoints(qpoints);

  // DexCache
  qpoints->pInitializeStaticStorage = art_quick_initialize_static_storage;
  qpoints->pInitializeTypeAndVerifyAccess = art_quick_initialize_type_and_verify_access;
  qpoints->pInitializeType = art_quick_initialize_type;
  qpoints->pResolveString = art_quick_resolve_string;

  // Field
  qpoints->pSet8Instance = wrap_art_quick_set8_instance;
  qpoints->pSet8Static = wrap_art_quick_set8_static;
  qpoints->pSet16Instance = wrap_art_quick_set16_instance;
  qpoints->pSet16Static = wrap_art_quick_set16_static;
  qpoints->pSet32Instance = wrap_art_quick_set32_instance;
  qpoints->pSet32Static = wrap_art_quick_set32_static;
  qpoints->pSet64Instance = wrap_art_quick_set64_instance;
  qpoints->pSet64Static = wrap_art_quick_set64_static;
  qpoints->pSetObjInstance = wrap_art_quick_set_obj_instance;
  qpoints->pSetObjStatic = wrap_art_quick_set_obj_static;
  qpoints->pGetByteInstance = wrap_art_quick_get_byte_instance;
  qpoints->pGetBooleanInstance = wrap_art_quick_get_boolean_instance;
  qpoints->pGetShortInstance = wrap_art_quick_get_short_instance;
  qpoints->pGetCharInstance = wrap_art_quick_get_char_instance;
  qpoints->pGet32Instance = wrap_art_quick_get32_instance;
  qpoints->pGet64Instance = wrap_art_quick_get64_instance;
  qpoints->pGetObjInstance = wrap_art_quick_get_obj_instance;
  qpoints->pGetByteStatic = wrap_art_quick_get_byte_static;
  qpoints->pGetBooleanStatic = wrap_art_quick_get_boolean_static;
  qpoints->pGetShortStatic = wrap_art_quick_get_short_static;
  qpoints->pGetCharStatic = wrap_art_quick_get_char_static;
  qpoints->pGet32Static = wrap_art_quick_get32_static;
  qpoints->pGet64Static = wrap_art_quick_get64_static;
  qpoints->pGetObjStatic = wrap_art_quick_get_obj_static;

  // Array
  qpoints->pAputObjectWithNullAndBoundCheck = wrap_art_quick_aput_obj_with_null_and_bound_check;
  qpoints->pAputObjectWithBoundCheck = wrap_art_quick_aput_obj_with_bound_check;
  qpoints->pAputObject = wrap_art_quick_aput_obj;
  qpoints->pHandleFillArrayData = wrap_art_quick_handle_fill_data;

  // JNI
  qpoints->pJniMethodStart = wrap_JniMethodStart;
  qpoints->pJniMethodStartSynchronized = wrap_JniMethodStartSynchronized;
  qpoints->pJniMethodEnd = wrap_JniMethodEnd;
  qpoints->pJniMethodEndSynchronized = wrap_JniMethodEndSynchronized;
  qpoints->pJniMethodEndWithReference = wrap_JniMethodEndWithReference;
  qpoints->pJniMethodEndWithReferenceSynchronized = wrap_JniMethodEndWithReferenceSynchronized;
  qpoints->pQuickGenericJniTrampoline = wrap_art_quick_generic_jni_trampoline;

  // Locks
  if (UNLIKELY(VLOG_IS_ON(systrace_lock_logging))) {
    qpoints->pLockObject = art_quick_lock_object_no_inline;
    qpoints->pUnlockObject = art_quick_unlock_object_no_inline;
  } else {
    qpoints->pLockObject = art_quick_lock_object;
    qpoints->pUnlockObject = art_quick_unlock_object;
  }

  // Invocation
  qpoints->pQuickImtConflictTrampoline = art_quick_imt_conflict_trampoline; // TODO: not manually wrapped
  qpoints->pQuickResolutionTrampoline = art_quick_resolution_trampoline; // TODO: not manually wrapped
  qpoints->pQuickToInterpreterBridge = art_quick_to_interpreter_bridge; // TODO: not manually wrapped
  qpoints->pInvokeDirectTrampolineWithAccessCheck =
      art_quick_invoke_direct_trampoline_with_access_check;
  qpoints->pInvokeInterfaceTrampolineWithAccessCheck =
      art_quick_invoke_interface_trampoline_with_access_check;
  qpoints->pInvokeStaticTrampolineWithAccessCheck =
      art_quick_invoke_static_trampoline_with_access_check;
  qpoints->pInvokeSuperTrampolineWithAccessCheck =
      art_quick_invoke_super_trampoline_with_access_check;
  qpoints->pInvokeVirtualTrampolineWithAccessCheck =
      art_quick_invoke_virtual_trampoline_with_access_check;

  // Thread
  qpoints->pTestSuspend = art_quick_test_suspend;

  // Throws
  qpoints->pDeliverException = art_quick_deliver_exception;
  qpoints->pThrowArrayBounds = art_quick_throw_array_bounds;
  qpoints->pThrowDivZero = art_quick_throw_div_zero;
  qpoints->pThrowNoSuchMethod = art_quick_throw_no_such_method;
  qpoints->pThrowNullPointer = art_quick_throw_null_pointer_exception;
  qpoints->pThrowStackOverflow = art_quick_throw_stack_overflow;

  // Deoptimize
  qpoints->pDeoptimize = art_quick_deoptimize_from_compiled_code;
};

}  // namespace art

#endif  // ART_RUNTIME_ENTRYPOINTS_QUICK_QUICK_DEFAULT_INIT_ENTRYPOINTS_H_
