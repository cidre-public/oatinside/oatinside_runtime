#include "touched_field_cache.h"

namespace art {
  TouchedFieldCache::TouchedFieldCache() {
    first = last = 0;
  }

  void TouchedFieldCache::add_entry(void* addr, std::tuple<mirror::Object*, mirror::Class*, ArtField*> value) {
    // If tab is full we have to remove the first inserted
    if((last + 1) % TOUCHED_FIELD_CACHE_SIZE == first) {
      map.erase(tab[first]);
      first = (first+1) % TOUCHED_FIELD_CACHE_SIZE;
    }
    tab[last] = addr;
    last = (last+1) % TOUCHED_FIELD_CACHE_SIZE;
    map.insert({addr, value});
  }

  void TouchedFieldCache::flush() {
    first = last = 0;
    map.clear();
  }

  std::tuple<mirror::Object*, mirror::Class*, ArtField*>* TouchedFieldCache::lookup(void* addr) {
    std::unordered_map<void*, std::tuple<mirror::Object*, mirror::Class*, ArtField*>>::iterator ret = map.find(addr);
    if(ret == map.end())
      return NULL;
    else
      return &ret->second;
  }
}
