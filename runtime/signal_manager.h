#ifndef ART_RUNTIME_SIGNAL_MANAGER_H_
#define ART_RUNTIME_SIGNAL_MANAGER_H_

#include <linux/types.h>
#include <map>
#include <signal.h>
#include <stack>
#include <vector>

#include "art_field.h"
#include "mirror/object.h"

#define DUMP_FILE_PATH "/data/local/tmp/dumped_mem/dump_no_"

namespace art {
  class SignalManager {
  private:   
    static void trapHandler(int signum, siginfo_t * info, void * ctx)
      NO_THREAD_SAFETY_ANALYSIS; /* TODO: hold mutator_lock_ */

    static void segvHandler(int signum, siginfo_t * info, void * ctx);
    
    pthread_mutex_t heap_mutex;

    struct sigaction oldactSEGV;
    struct sigaction oldactTRAP;

#if defined(__arm__) || defined(__aarch64__)
    int stx_status_register; // 0 -> 31: number, -1: not stx
    void* sandbox_return_addr;
    void* sandbox;
#endif

#if defined(__aarch64__)
    pthread_mutex_t excl_addr_vector_mutex;
    std::vector<void*> last_exclusive_addr_vector;
#endif

  public:
    void Init();

    /* Returns a dump ID > 0 (strictly) */
    unsigned int DumpProccessToFile(bool threads_already_paused);
    
#if defined(__aarch64__)
    static struct fpsimd_context* GetFpsimdFromContext(const ucontext_t* ctx);
#endif

    // Init in ProbeManager::NewThread
    std::vector<std::stack<void*>*> wrapper_lr;

    friend class ProbeManager;
  };
}

extern "C" void* do_wrap_exit()
  NO_THREAD_SAFETY_ANALYSIS; /* TODO: hold mutator_lock_ */

extern "C" void do_wrap_enter(void* lr)
  NO_THREAD_SAFETY_ANALYSIS; /* TODO: hold mutator_lock_ */

#endif  // ART_RUNTIME_SIGNAL_MANAGER_H_
