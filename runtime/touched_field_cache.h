#ifndef ART_RUNTIME_TOUCHED_FIELD_CACHE_H_
#define ART_RUNTIME_TOUCHED_FIELD_CACHE_H_

#include <tuple>
#include <unordered_map>

#define TOUCHED_FIELD_CACHE_SIZE 512

namespace art {

  namespace mirror {
    class Object;
    class Class;
  }
  class ArtField;

  class TouchedFieldCache {
  private:
    std::unordered_map<void*, std::tuple<mirror::Object*, mirror::Class*, ArtField*>> map;
    void* tab[TOUCHED_FIELD_CACHE_SIZE];
    unsigned long first;
    unsigned long last;
  public:
    TouchedFieldCache();
    void add_entry(void* addr, std::tuple<mirror::Object*, mirror::Class*, ArtField*> value);
    void flush();
    std::tuple<mirror::Object*, mirror::Class*, ArtField*>* lookup(void* addr);
  };
}

#endif
