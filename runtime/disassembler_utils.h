#ifndef ART_RUNTIME_DISASSEMBLER_UTILS_H_
#define ART_RUNTIME_DISASSEMBLER_UTILS_H_

#if defined(__aarch64__)
bool is_arm64_ldx_instruction(unsigned int instr);
bool is_arm64_stx_instruction(unsigned int instr);
unsigned int get_corresponding_arm64_not_exclusive_store(unsigned int store_instr, int* status_register);
#endif

#endif  // ART_RUNTIME_DISASSEMBLER_UTILS_H_
