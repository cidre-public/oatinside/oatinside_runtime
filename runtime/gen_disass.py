from collections import namedtuple

# Mask is a string (size 32) with 1, 0 or x for unconstrainted
# variables is a map between name and ranges (all bornes included)
Instr = namedtuple('Instr', ['name', 'mask', 'variables'])
pairedSts = []
lds = []

LDXP = Instr('ldxp', '1x001000011xxxxx0xxxxxxxxxxxxxxx',
             { 'size': (30,30),
               'Rs': (16,20),
               'Rt2': (10,14),
               'Rn': (5,9),
               'Rt': (0,4) })
lds.append(LDXP)

LDXR = Instr('ldxr', '1x001000010xxxxx0xxxxxxxxxxxxxxx',
             { 'size': (30,30),
               'Rs': (16,20),
               'Rt2': (10,14),
               'Rn': (5,9),
               'Rt': (0,4) })
lds.append(LDXR)

LDXRB = Instr('ldxrb', '00001000010xxxxx0xxxxxxxxxxxxxxx',
             { 'Rs': (16,20),
               'Rt2': (10,14),
               'Rn': (5,9),
               'Rt': (0,4) })
lds.append(LDXRB)

LDXRH = Instr('ldxrh', '01001000010xxxxx0xxxxxxxxxxxxxxx',
             { 'Rs': (16,20),
               'Rt2': (10,14),
               'Rn': (5,9),
               'Rt': (0,4) })
lds.append(LDXRH)

LDAXP = Instr('ldaxp', '1x001000011xxxxx1xxxxxxxxxxxxxxx',
             { 'size': (30,30),
               'Rs': (16,20),
               'Rt2': (10,14),
               'Rn': (5,9),
               'Rt': (0,4) })
lds.append(LDAXP)

LDAXR = Instr('ldaxr', '1x001000010xxxxx1xxxxxxxxxxxxxxx',
             { 'size': (30,30),
               'Rs': (16,20),
               'Rt2': (10,14),
               'Rn': (5,9),
               'Rt': (0,4) })
lds.append(LDAXR)

LDAXRB = Instr('ldaxrb', '00001000010xxxxx1xxxxxxxxxxxxxxx',
             { 'Rs': (16,20),
               'Rt2': (10,14),
               'Rn': (5,9),
               'Rt': (0,4) })
lds.append(LDAXRB)

LDAXRH = Instr('ldaxrh', '01001000010xxxxx1xxxxxxxxxxxxxxx',
             { 'Rs': (16,20),
               'Rt2': (10,14),
               'Rn': (5,9),
               'Rt': (0,4) })
lds.append(LDAXRH)

STXP = Instr('stxp', '1x001000001xxxxx0xxxxxxxxxxxxxxx',
             { 'size': (30,30),
               'Rs': (16,20),
               'Rt2': (10,14),
               'Rn': (5,9),
               'Rt': (0,4) })
STP = Instr('stp', 'x010100010xxxxxxxxxxxxxxxxxxxxxx',
             { 'size': (31,31),
               'imm7': (15,21),
               'Rt2': (10,14),
               'Rn': (5,9),
               'Rt': (0,4) })
pairedSts.append((STXP, STP))

STXR = Instr('stxr', '1x001000000xxxxx0xxxxxxxxxxxxxxx',
             { 'size': (30,30),
               'Rs': (16,20),
               'Rt2': (10,14),
               'Rn': (5,9),
               'Rt': (0,4) })
STR = Instr('str', '1x111000000xxxxxxxxx01xxxxxxxxxx',
             { 'size': (30,30),
               'imm9': (12,20),
               'Rn': (5,9),
               'Rt': (0,4) })
pairedSts.append((STXR, STR))

STXRB = Instr('stxrb', '00001000000xxxxx0xxxxxxxxxxxxxxx',
             { 'Rs': (16,20),
               'Rt2': (10,14),
               'Rn': (5,9),
               'Rt': (0,4) })
STRB = Instr('strb', '00111000000xxxxxxxxx01xxxxxxxxxx',
             { 'imm9': (12,20),
               'Rn': (5,9),
               'Rt': (0,4) })
pairedSts.append((STXRB, STRB))

STXRH = Instr('stxrh', '01001000000xxxxx0xxxxxxxxxxxxxxx',
             { 'Rs': (16,20),
               'Rt2': (10,14),
               'Rn': (5,9),
               'Rt': (0,4) })
STRH = Instr('strh', '01111000000xxxxxxxxx01xxxxxxxxxx',
             { 'imm9': (12,20),
               'Rn': (5,9),
               'Rt': (0,4) })
pairedSts.append((STXRH, STRH))

STLXP = Instr('stlxp', '1x001000001xxxxx1xxxxxxxxxxxxxxx',
             { 'size': (30,30),
               'Rs': (16,20),
               'Rt2': (10,14),
               'Rn': (5,9),
               'Rt': (0,4) })
pairedSts.append((STLXP, STP))
# Nt: STLP does not exist

STLXR = Instr('stlxr', '1x001000000xxxxx1xxxxxxxxxxxxxxx',
             { 'size': (30,30),
               'Rs': (16,20),
               'Rt2': (10,14),
               'Rn': (5,9),
               'Rt': (0,4) })
STLR = Instr('stlr', '1x001000100xxxxx1xxxxxxxxxxxxxxx',
             { 'size': (30,30),
               'Rs': (16,20),
               'Rt2': (10,14),
               'Rn': (5,9),
               'Rt': (0,4) })
pairedSts.append((STLXR, STLR))

STLXRB = Instr('stlxb', '00001000000xxxxx1xxxxxxxxxxxxxxx',
             { 'Rs': (16,20),
               'Rt2': (10,14),
               'Rn': (5,9),
               'Rt': (0,4) })
STLRB = Instr('stlrb', '00001000100xxxxx1xxxxxxxxxxxxxxx',
             { 'Rs': (16,20),
               'Rt2': (10,14),
               'Rn': (5,9),
               'Rt': (0,4) })
pairedSts.append((STLXRB, STLRB))

STLXRH = Instr('stlxrh', '01001000000xxxxx1xxxxxxxxxxxxxxx',
             { 'Rs': (16,20),
               'Rt2': (10,14),
               'Rn': (5,9),
               'Rt': (0,4) })
STLRH = Instr('stlrh', '01001000100xxxxx1xxxxxxxxxxxxxxx',
             { 'Rs': (16,20),
               'Rt2': (10,14),
               'Rn': (5,9),
               'Rt': (0,4) })
pairedSts.append((STLXRH, STLRH))

def gen_if(instr, variable_name, shift):
    mask = instr.mask

    print '%s// test if %s is %s' % (shift, variable_name, instr.name)
    print '%sif((%s & 0b%s) == 0b%s)' % (shift, variable_name, mask.replace('0','1').replace('x','0'), mask.replace('x', '0')),

def gen_transpo(stx, st, variable_name, out_name, shift):
    # Replace all x by zero so S, Rm and option field are ignored
    base_instr = st.mask.replace('x', '0')

    print '%s// Generating %s instr\n' % (shift, st.name)
    print '%s%s = 0b%s;\n' % (shift, out_name, base_instr)
    
    for name, (inf, sup) in st.variables.iteritems():
        # This field are ignored because not used in stX instructions
        if name not in ['Rm', 'option', 'S', 'imm7', 'imm9']:
            xinf, xsup = stx.variables[name]
            nb_bits = xsup - xinf + 1
            mask = ((1 << nb_bits) - 1) << xinf

            print '%s// Get %s field' % (shift, name)
            if xsup - sup != 0:
                if xsup > sup:
                    print '%s%s |= (%s & 0x%x) >> %i;\n' % (shift, out_name, variable_name, mask, xsup - sup)
                else:
                    print '%s%s |= (%s & 0x%x) << %i;\n' % (shift, out_name, variable_name, mask, sup - xsup)
            else:
                print '%s%s |= (%s & 0x%x);\n' % (shift, out_name, variable_name, mask)

    (inf, sup) = stx.variables['Rs']
    nb_bits = sup - inf + 1
    mask = ((1 << nb_bits) - 1) << inf
    print "%s// Get status register number" % shift
    print "%s*status_register = ((%s & %i) >> %i);\n" % (shift, variable_name, mask, inf)

print "/* WARNING: automatically generated by gen_disass.py*/\n\n"

# Generate all load condition
print 'bool is_arm64_ldx_instruction(unsigned int instr) {'
for ld in lds:
    gen_if(ld, 'instr', shift='  ')
    print '\n    return true;'
print "  return false;\n}\n"

# Generate all store condition
print 'bool is_arm64_stx_instruction(unsigned int instr) {'
for (stx,_) in pairedSts:
    gen_if(stx, 'instr', shift='  ')
    print '\n    return true;'
print "  return false;\n}\n"

# Generate all transformation
print 'unsigned int get_corresponding_arm64_not_exclusive_store(unsigned int store_instr, int* status_register) {'
print '  unsigned int new_instr;\n'
for (stx, st) in pairedSts:
    gen_if(stx, 'store_instr', shift='  ')
    print ' {'
    gen_transpo(stx, st, 'store_instr', 'new_instr', shift='    ')
    print '    return new_instr;\n  }'
print '  return 0;\n}\n'

